Bibliothèques utilisées avec explication:


Tkinter : Création d'une GUI simple, à partir de Frame sur lequel on ajoute des widgets, dans eux-mêmes sont placés les boutons et labels avec texte. 

import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox

import psutil : Extraction données système sur CPU, RAM, Disk et Network (pour OS Windows) 
import cpuinf : Extraction nom / modèle du processeur

import matplotlib.pyplot as plt : Tracé graphique statique (plot) à partir de points à coordonnées (x,y)
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg : Mise en canvas du graphique animé pour rajouter dans un widget tkinter
from matplotlib.animation import FuncAnimation : Fonction pour dynamiser le graphique statique en le mettant à jour avec nouvelles valeurs x et y

import datetime as dt : Extraire la date actuelle
from datetime import date
import time : Extraire l'heure actuelle
import calendar : Déterminer le jour de la semaine actuel

from PIL import Image, ImageTk : Formater une image avant de l'insérer dans un label / bouton tkinter
 
import socket : Tester connexion à Internet en se connectant à une page Internet.