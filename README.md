# Guide visuel du Python Task Monitor

Bienvenue dans le guide visuel du Python Task Monitor ! Cette section vous aidera à mieux comprendre l'organisation visuelle du projet et à naviguer facilement à travers les différents répertoires et fichiers.

### Structure du projet :

- **sources/** : Ce répertoire contient tous les codes sources nécessaires pour le fonctionnement du Task Monitor. Vous y trouverez le fichier principal `task_monitor.py` ainsi que d'autres modules et fichiers nécessaires à l'exécution de l'application.

- **doc/** : Ce répertoire est dédié à la documentation technique du projet. Vous pouvez y trouver des informations détaillées sur l'architecture du Task Monitor, les décisions de conception, ainsi que des guides pour les développeurs souhaitant contribuer au projet.

### Navigation rapide :

- **README.md** : Le fichier README contient le protocole d'utilisation du Task Monitor. C'est ici que vous trouverez des instructions pour démarrer l'application sur OS Windows (uniquement Windows;  pas de possiblité de lancement sur Mac, Linux et autres OS), ainsi que d'autres informations importantes sur le projet.

- **requirements.txt** : Ce fichier répertorie tous les prérequis nécessaires au lancement et au fonctionnement du Task Monitor. Assurez-vous d'avoir installé toutes les dépendances répertoriées avant d'exécuter l'application.

### Protocole d'utilisation :

Pour lancer le Python Task Monitor sur votre système Windows :

1. Assurez-vous d'avoir Python installé.
2. Clonez ce dépôt sur votre machine.
3. Naviguez jusqu'au répertoire contenant les codes sources : `cd sources/`
4. Installez les dépendances requises : `pip install -r requirements.txt`
5. Lancez l'application : `main.py`
6. Passez en mode pleine écran la fenêtre.
7. Cliquer sur le bouton "Get started".
8. Cliquer sur les différentes rubriques pour naviguer sur le Python Task Monitor.

### Besoin d'aide ?

Si vous avez des questions sur la navigation dans le projet ou sur son utilisation, n'hésitez pas à consulter la documentation ou à ouvrir une issue sur GitHub. Nous sommes là pour vous aider !

Profitez pleinement de votre expérience avec le Python Task Monitor !
