# PROJET PYTHON TASK MONITOR



# Bibliothèques Python utilisées (regroupées par leur fonction dans le programme)
import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox

import psutil
import cpuinfo

import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.animation import FuncAnimation

import datetime as dt
from datetime import date
import time
import calendar

from PIL import Image, ImageTk

import multiprocessing
import socket


#=======================================FUNCTIONS==========================================================
# Définition de fonctions utilisées au sein des boutons


def bytes_to_human_readable(num, suffix='B'):
    """
    (Fonction empruntée sur Internet)
    Renvoie le nombre de bytes convertis en avec un suffixe.

    Paramètre: num : (int), le nombre de bytes
    Retour: (str)
    """
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num) < 1024.0:
            return f"{num:3.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"

def change_color(button_name):
    """
    Change la couleur d'un bouton lorsque celui-ci est cliqué.
    """
    button_name.config(bg="#FFFFFF")
    button_name.config(bg="#ebebeb")



"""
Fonctions pour la rubrique "Performances" avec même structure de code pour chaque élément parmi
CPU, Memory, Disk et Network. 

Structure suivante: une procédure plot_élément() qui construit le graphique en temps réel 
montrant l'évolution en fonction du temps des grandeurs mesurées pour chaque élément du système, 
dans la fonction update_plot(). 

En même temps et dans la même fonction update_plot() sont en train de se modifier systématiquement 
les valeurs pour les grandeurs mesurées pour d'autres éléments du système, et qui sont affichées 
dans des labels.
"""



def plot_cpu():
    """
    Procédure pour tracer le graphique du pourcentage de CPU utilisé en fonction du temps.

    Modifie également les valeurs écrites dans les Labels pour différentes données statistiques.

    Pourcentage batterie, date et heure sont également modifiées à itération de la fonction update_plot()
    au sein de l'animation Animate Func du module Matplotlib.

    Plusieurs variables de portée globale utilisées et dont les valeurs sont modifiées en permanence
    lors de l'appel de la procédure. 
    """
    global anim, percent_cpu, cpu, memory, Timer, disk, Battery, network, gpu
    global processes, numThreads, handles
    global CPU_realtime_txt, CPU_realtime, CPU_speed_txt, CPU_speed, CPU_processes_txt, CPU_processes
    global CPU_threads_txt, CPU_threads, CPU_handles_txt, CPU_handles
    global cores_txt, cores
    global num_switches, num_interrupts, num_syscalls
    global cpu_average, CPU_uptime


    fig, ax = plt.subplots(figsize=(10, 6.5), facecolor="black")
    fig.subplots_adjust(left=0.075, bottom=0.01, right=0.95, top=0.95, wspace=None, hspace=None)
    x = [i for i in range(60, -1, -1)]
    y = []

    def update_plot(i, x, y):
        """
        Fonction qui permet de tracer le graphique du CPU

        Paramètres:
        x (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y (list): valeur mesurée pour le pourcentage CPU (61 valeurs max, même principe
         de 'update' que la liste x.
        """
        global percent_cpu, x_cpu, y_cpu, processes, numThreads, handles

        percent = psutil.cpu_percent(1)
        percent_cpu = percent
        y.append(percent)

        average = round(sum(y) / len(y),1)

        #Date_time
        today = date.today()
        d1 = today.strftime("%d/%m/%Y")
        day_week = calendar.day_name[today.weekday()][:3]

        time1 = dt.datetime.now().strftime('%H:%M:%S')

        def secs2hours(secs):
            """
            Convertir les secondes en heures, minutes et secondes

            Paramètres: secs (int)
            Retour: heure sous forme hh:mm:ss (str)
            """
            mm, ss = divmod(secs, 60)
            hh, mm = divmod(mm, 60)
            return "%d h %02d min %02d s" % (abs(hh), mm, ss)

        battery = psutil.sensors_battery()
        percentage = battery.percent
        time_bat = battery.secsleft

        battery_image = Image.open(f"Batterie/Batterie_Pourcentage_{percentage}.png")
        battery_image = battery_image.resize((50, 35), Image.Resampling.LANCZOS)
        photo = ImageTk.PhotoImage(battery_image)

        AC_power = psutil.sensors_battery()[2]
        status = "⌁ Charging    "
        if AC_power is False or None:
            status = "Not Charging"

        Battery.configure(text=f"Time Left: {secs2hours(time_bat)}            {day_week} {d1}\nStatus: {status}             "
                               f"                    {time1}",
                          image=photo, compound="left", anchor=E)
        Battery.image = photo

        freq = round(psutil.cpu_freq()[0] / 10**3, 2)

        processes = 0
        numThreads = 0
        handles = 0
        threads()

        CPU_threads.config(text=f"{numThreads}", anchor=W)

        CPU_processes.config(text=f"{processes}", anchor=W)

        CPU_handles.config(text=f"{handles}", anchor=W)

        for i in range(psutil.cpu_count()):
            cores[i].config(text=f"{psutil.cpu_percent(interval=0, percpu=True)[i]} %", anchor=E)

        battery_image1 = Image.open("Icones/CPU_icone.png")
        battery_image1 = battery_image1.resize((110, 90), Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo1 = ImageTk.PhotoImage(battery_image1)

        cpu.config(text = f"CPU\n{percent} % {freq} GHz", justify = LEFT, anchor = W, image=photo1,
                   compound = "left")

        Battery.image1 = photo1

        free_memory = round(psutil.virtual_memory()[3] / 10 ** 9, 1)
        total_memory = round(psutil.virtual_memory()[0] / 10 ** 9, 1)
        percent_memory = psutil.virtual_memory()[2]

        battery_image2 = Image.open("Icones/Ram_icone.png")
        battery_image2 = battery_image2.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo2 = ImageTk.PhotoImage(battery_image2)

        memory.config(text=f"Memory\n{free_memory} / {total_memory} GB ({percent_memory} %)",
                      justify=LEFT, anchor=W, image = photo2, compound = "left")

        Battery.image2 = photo2

        disk_load = psutil.disk_usage("C:\\")[3]

        battery_image3 = Image.open("Icones/Disk_icone.png")
        battery_image3 = battery_image3.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo3 = ImageTk.PhotoImage(battery_image3)

        disk.config(text=f"Disk 0 (C:)\n{disk_load} %", justify=LEFT, anchor=W,
                    image = photo3, compound = "left")

        Battery.image3 = photo3

        #Wi-Fi button configuration
        recv_bytes = psutil.net_io_counters().bytes_recv
        sent_bytes = psutil.net_io_counters().bytes_sent
        time.sleep(1)
        recv_bytes1 = psutil.net_io_counters().bytes_recv
        sent_bytes1 = psutil.net_io_counters().bytes_sent
        recv_rate = (recv_bytes1 - recv_bytes) * 0.008
        sent_rate = (sent_bytes1 - sent_bytes) * 0.008

        def check_internet_connection():
            """
            Procédure vérifiant la connexion à Internet pour choisir l'image correspondate
            à la connexion ou non à ce-dernier.
            """
            remote_server = "www.google.com"
            port = 80
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            try:
                sock.connect((remote_server, port))
                return True
            except socket.error:
                return False
            finally:
                sock.close()

        if check_internet_connection():
            battery_image4 = Image.open("Icones/WifiOn_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4
        else:
            battery_image4 = Image.open("Icones/WifiOff_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4

        #GPU button configuration
        battery_image5 = Image.open("Icones/GPU_icone.png")
        battery_image5 = battery_image5.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo5 = ImageTk.PhotoImage(battery_image5)

        gpu.config(text=f"GPU", justify=LEFT, anchor=W,
                       image=photo5, compound="left")

        Battery.image5 = photo5


        CPU_realtime.config(text = f"{percent} % \n", anchor = W)
        CPU_speed.config(text = f"{freq} GHz \n", anchor = W)

        #Trivia
        num_switches.config(text=f"{psutil.cpu_stats()[0]}")
        num_interrupts.config(text=f"{psutil.cpu_stats()[1]}")
        num_syscalls.config(text=f"{psutil.cpu_stats()[3]}")
        cpu_average.config(text=f"{average} %")

        #Uptime
        def seconds_elapsed():
            return time.time() - psutil.boot_time()

        def change(time):
            day = time // (24 * 3600)
            time = time % (24 * 3600)
            hour = time // 3600
            time %= 3600
            minutes = time // 60
            time %= 60
            seconds = time
            return "%d:%d:%d:%d" % (day, hour, minutes, seconds)
        uptime = change(seconds_elapsed())
        CPU_uptime.config(text=f"{uptime}\n")

        x = x[-61:]
        y = y[-61:]
        x = x[-len(y):]
        y = y[-len(y):]

        ax.clear()
        ax.plot(x, y, color="blue")
        ax.fill_between(x, y, 0, color='blue', alpha=.1)

        ax.spines["bottom"].set_color("white")
        ax.spines["left"].set_color("white")
        ax.tick_params(axis="x", colors="white")
        ax.tick_params(axis="y", colors="white")
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.title.set_color('white')
        ax.set_ylim([0, 101])
        ax.set_xlim([60, 0])
        ax.set_facecolor("black")

        plt.grid(color='c', linewidth=0.2)

        plt.subplots_adjust(bottom=0.2)
        plt.margins(x=0)
        ax.set_xlabel('Time Period, s')
        ax.set_ylabel('CPU Utilization, %')

    anim = FuncAnimation(fig, update_plot, fargs=(x, y), interval=250)
    canvas = FigureCanvasTkAgg(fig, master=initial_screen)
    canvas.get_tk_widget().grid(row = 3, column = 1, columnspan= 3)

def threads():
    """
    Procédure qui calcule le nombre de threads et celui de handles total, calculé en tant que la
    somme respective des threads et de handles pour chacun des processus tournant sur les système.
    """
    global processes, numThreads, handles
    for proc in psutil.process_iter(["num_threads", "num_handles"]):
        processes += 1
        numThreads += proc.info["num_threads"]
        handles += proc.info["num_handles"]



def plot_memory():
    """
    Procédure pour tracer le graphique de la taille de Memoire utilisée (en MB) en fonction du temps.

    Modifie également les valeurs écrites dans les Labels pour différentes données statistiques.

    Pourcentage batterie, date et heure sont également modifiées à itération de la procédure update_plot()
    au sein de l'animation Animate Func du module Matplotlib.

    Plusieurs variables de portée globale utilisées et dont les valeurs sont modifiées en permanence
    lors de l'appel de la procédure.
    """
    global anim1, cpu, percent_cpu, memory, disk, Battery, disk, network, gpu, menu
    global in_use, available, percent_ram, total, free
    global total_swap, swap_used, swap_percent, swap_free, swap_sin, swap_sout
    global average_percent
    fig, ax = plt.subplots(figsize=(10, 6.5), facecolor="black")
    fig.subplots_adjust(left=0.075, bottom=0.01, right=0.95, top=0.95, wspace=0, hspace=0)

    x = [i for i in range(60, -1, -1)]
    y = []
    perc = []

    def update_plot(i, x, y, perc):
        """
        Fonction qui permet de tracer le graphique de la RAM

        Paramètres:
        x (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y (list): valeur mesurée pour la place de la RAM utilisée (61 valeurs max, même principe
         de 'update' que la liste x.
        perc (list): valeur mesurée le pourcentage de la RAM utilisé (61 valeurs max, FILO).
        """
        percent = round(psutil.virtual_memory()[3] / 10**9, 1)
        percent1 = psutil.virtual_memory()[2]
        perc.append(percent1)
        y.append(percent)

        percent_cpu = psutil.cpu_percent(1)
        freq = round(psutil.cpu_freq()[0] / 10 ** 3, 2)

        #General info
        battery_image1 = Image.open("Icones/CPU_icone.png")
        battery_image1 = battery_image1.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo1 = ImageTk.PhotoImage(battery_image1)

        cpu.config(text=f"CPU\n{percent_cpu} % {freq} GHz", justify=LEFT, anchor=W, image=photo1,
                   compound="left")

        Battery.image1 = photo1

        free_memory = round(psutil.virtual_memory()[3] / 10 ** 9, 1)
        total_memory = round(psutil.virtual_memory()[0] / 10 ** 9, 1)
        percent_memory = psutil.virtual_memory()[2]

        battery_image2 = Image.open("Icones/Ram_icone.png")
        battery_image2 = battery_image2.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo2 = ImageTk.PhotoImage(battery_image2)

        memory.config(text=f"Memory\n{free_memory} / {total_memory} GB ({percent_memory} %)",
                      justify=LEFT, anchor=W, image=photo2, compound="left")

        Battery.image2 = photo2

        disk_load = psutil.disk_usage("C:\\")[3]

        battery_image3 = Image.open("Icones/Disk_icone.png")
        battery_image3 = battery_image3.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo3 = ImageTk.PhotoImage(battery_image3)

        disk.config(text=f"Disk 0 (C:)\n{disk_load} %", justify=LEFT, anchor=W,
                    image=photo3, compound="left")

        Battery.image3 = photo3

        # Wi-Fi button configuration
        recv_bytes = psutil.net_io_counters().bytes_recv
        sent_bytes = psutil.net_io_counters().bytes_sent
        time.sleep(1)
        recv_bytes1 = psutil.net_io_counters().bytes_recv
        sent_bytes1 = psutil.net_io_counters().bytes_sent
        recv_rate = (recv_bytes1 - recv_bytes) * 0.008
        sent_rate = (sent_bytes1 - sent_bytes) * 0.008

        def check_internet_connection():
            """
            Vérifier connexion internet (procédure) pour choisir image correspondante s'il y a ou non
            connexion.
            """
            remote_server = "www.google.com"
            port = 80
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            try:
                sock.connect((remote_server, port))
                return True
            except socket.error:
                return False
            finally:
                sock.close()

        if check_internet_connection():
            battery_image4 = Image.open("Icones/WifiOn_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4
        else:
            battery_image4 = Image.open("Icones/WifiOff_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4

        # GPU button configuration
        battery_image5 = Image.open("Icones/GPU_icone.png")
        battery_image5 = battery_image5.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo5 = ImageTk.PhotoImage(battery_image5)

        gpu.config(text=f"GPU", justify=LEFT, anchor=W,
                   image=photo5, compound="left")

        Battery.image5 = photo5



        #Battery_Daytime_info
        today = date.today()
        d1 = today.strftime("%d/%m/%Y")
        day_week = calendar.day_name[today.weekday()][:3]

        time1 = dt.datetime.now().strftime('%H:%M:%S')

        def secs2hours(secs):
            """
            Convertir s au format hh.mm.ss (cf plot_cpu())
            """
            mm, ss = divmod(secs, 60)
            hh, mm = divmod(mm, 60)
            return "%d h %02d min %02d s" % (abs(hh), mm, ss)

        battery = psutil.sensors_battery()
        percentage = battery.percent
        time_bat = battery.secsleft

        battery_image = Image.open(f"Batterie/Batterie_Pourcentage_{percentage}.png")
        battery_image = battery_image.resize((50, 35),
                                             Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo = ImageTk.PhotoImage(battery_image)

        AC_power = psutil.sensors_battery()[2]
        status = "⌁ Charging    "
        if AC_power is False or None:
            status = "Not Charging"

        Battery.configure(
            text=f"Time Left: {secs2hours(time_bat)}            {day_week} {d1}\nStatus: {status}             "
                 f"                    {time1}",
            image=photo, compound="left", anchor=E)
        Battery.image = photo

        #RAM INFO
        RAM_in_use = round(psutil.virtual_memory()[3] / 10**9, 1)
        RAM_available = round(psutil.virtual_memory()[1] / 10**9, 1)
        RAM_percent_ram = psutil.virtual_memory()[2]
        RAM_total = round(psutil.virtual_memory()[0] / 10**9, 1)
        RAM_free = round(psutil.virtual_memory()[4] / 10**9, 1)

        in_use.config(text=f"{RAM_in_use} GB")
        available.config(text=f"{RAM_available} GB")
        percent_ram.config(text=f"{RAM_percent_ram} %")
        total.config(text=f"{RAM_total} GB")
        free.config(text=f"{RAM_free} GB")
        
        try:
            RAM_total_swap = round(psutil.swap_memory()[0] / 10**9, 1)
        except RuntimeError:
            RAM_total_swap = "No data available"
            
        try:
            RAM_swap_used = round(psutil.swap_memory()[1] / 10 ** 9, 1)
        except RuntimeError:
            RAM_swap_used = "No data available"
        
        try:
            RAM_swap_percent = round(psutil.swap_memory()[3] / 10 ** 9, 1)
        except RuntimeError:
            RAM_swap_percent = "No data available"
            
        try:
            RAM_swap_free = round(psutil.swap_memory()[2] / 10 ** 9, 1)
        except RuntimeError:
            RAM_swap_free = "No data available"
            
        try:
            RAM_swap_sin = round(psutil.swap_memory()[4] / 10 ** 9, 1)
        except RuntimeError:
            RAM_swap_sin = "No data available"
            
        try:
            RAM_swap_sout = round(psutil.swap_memory()[5] / 10 ** 9, 1)
        except RuntimeError:
            RAM_swap_sout = "No data available"
        
        """
        try:
            RAM_total_swap = round(psutil.swap_memory()[0] / 10**9, 1)
            RAM_swap_used = round(psutil.swap_memory()[1] / 10 ** 9, 1)
            RAM_swap_percent = round(psutil.swap_memory()[3] / 10 ** 9, 1)
            RAM_swap_free = round(psutil.swap_memory()[2] / 10 ** 9, 1)
            RAM_swap_sin = round(psutil.swap_memory()[4] / 10 ** 9, 1)
            RAM_swap_sout = round(psutil.swap_memory()[5] / 10 ** 9, 1)
        except:
            RAM_total_swap = "No data available"
            RAM_swap_used = "No data available"
            RAM_swap_percent = "No data available"
            RAM_swap_free = "No data available"
            RAM_swap_sin = "No data available"
            RAM_swap_sout = "No data available"
        """
        

        #TRIVIA

        total_swap.config(text=f"{RAM_total_swap}")
        swap_used.config(text=f"{RAM_swap_used}")
        swap_percent.config(text=f"{RAM_swap_percent}")
        swap_free.config(text=f"{RAM_swap_free}")
        swap_sin.config(text=f"{RAM_swap_sin}")
        swap_sout.config(text=f"{RAM_swap_sout}")

        #Average
        average_percent.config(text=f"{round(sum(perc) / len(perc), 1)} %")

        x = x[-61:]
        y = y[-61:]
        x = x[-len(y):]
        y = y[-len(y):]
        perc = perc[-61:]
        perc = perc[-len(perc):]

        ax.clear()
        ax.plot(x, y, color="red")
        ax.fill_between(x, y, 0, color='red', alpha=.1)

        ax.spines["bottom"].set_color("white")
        ax.spines["left"].set_color("white")
        ax.tick_params(axis="x", colors="white")
        ax.tick_params(axis="y", colors="white")
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.title.set_color('white')
        ax.set_ylim([0, round(psutil.virtual_memory()[0] / 10**9, 1)])
        ax.set_xlim([60, 0])
        ax.set_facecolor("black")

        plt.grid(color='c', linewidth=0.2)

        plt.subplots_adjust(bottom=0.20)
        ax.set_xlabel('Time Period, s')
        ax.set_ylabel('RAM Used, GB')

    anim1 = FuncAnimation(fig, update_plot, fargs=(x, y, perc), interval=250)
    canvas = FigureCanvasTkAgg(fig, master=initial_screen)
    canvas.get_tk_widget().grid(row = 3, column = 1, columnspan= 3)

def plot_disk():
    """
    Procédure pour tracer 2 graphiques:
    - Pourcentage du disque C utilisé à chaque seconde
    - Bitrate (vitesse) de la lecture et de l'écriture des données sur ce disque (en MB/s)

    Modifie également les valeurs écrites dans les Labels pour différentes données statistiques.

    Pourcentage batterie, date et heure sont également modifiées à itération de la procédure update_plot()
    au sein de l'animation Animate Func du module Matplotlib.

    Plusieurs variables de portée globale utilisées et dont les valeurs sont modifiées en permanence
    lors de l'appel de la procédure.
    """
    global ani, disk, cpu, percent_cpu, memory, disk, Battery, disk, network, gpu, menu
    global Disk_used, Disk_total, Disk_percent, Disk_free
    global disk_read_speed, disk_read_count, disk_read_time, disk_write_speed, disk_write_count, disk_write_time
    global read_average, write_average

    def my_function(i, x, y, x1, y1, x2, y2):
        """
        Fonction qui permet de tracer le graphique du disque C

        Paramètres:
        x (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y (list): valeur mesurée pour le pourcentage du disque C utilisé (61 valeurs max, même principe
         de 'update' que la liste x.
        x1 (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y1 (list): valeur mesurée pour la vitesse de le lecture sur disque C (61 valeurs max, même principe
         de 'update' que la liste x.
        x2 (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y2 (list): valeur mesurée pour la vitesse de l'écriture' sur disque C (61 valeurs max, même principe
         de 'update' que la liste x.
        """
        percent_cpu = psutil.cpu_percent(1)
        freq = round(psutil.cpu_freq()[0] / 10 ** 3, 2)

        # General info
        battery_image1 = Image.open("Icones/CPU_icone.png")
        battery_image1 = battery_image1.resize((110, 90), Image.Resampling.LANCZOS)
        photo1 = ImageTk.PhotoImage(battery_image1)

        cpu.config(text=f"CPU\n{percent_cpu} % {freq} GHz", justify=LEFT, anchor=W, image=photo1,
                   compound="left")

        Battery.image1 = photo1

        free_memory = round(psutil.virtual_memory()[3] / 10 ** 9, 1)
        total_memory = round(psutil.virtual_memory()[0] / 10 ** 9, 1)
        percent_memory = psutil.virtual_memory()[2]

        battery_image2 = Image.open("Icones/Ram_icone.png")
        battery_image2 = battery_image2.resize((110, 90), Image.Resampling.LANCZOS)
        photo2 = ImageTk.PhotoImage(battery_image2)

        memory.config(text=f"Memory\n{free_memory} / {total_memory} GB ({percent_memory} %)",
                      justify=LEFT, anchor=W, image=photo2, compound="left")

        Battery.image2 = photo2

        disk_load = psutil.disk_usage("C:\\")[3]

        battery_image3 = Image.open("Icones/Disk_icone.png")
        battery_image3 = battery_image3.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo3 = ImageTk.PhotoImage(battery_image3)

        disk.config(text=f"Disk 0 (C:)\n{disk_load} %", justify=LEFT, anchor=W,
                    image=photo3, compound="left")

        Battery.image3 = photo3

        # Wi-Fi button configuration
        recv_bytes = psutil.net_io_counters().bytes_recv
        sent_bytes = psutil.net_io_counters().bytes_sent
        time.sleep(1)
        recv_bytes1 = psutil.net_io_counters().bytes_recv
        sent_bytes1 = psutil.net_io_counters().bytes_sent
        recv_rate = (recv_bytes1 - recv_bytes) * 0.008
        sent_rate = (sent_bytes1 - sent_bytes) * 0.008

        def check_internet_connection():
            remote_server = "www.google.com"
            port = 80
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            try:
                sock.connect((remote_server, port))
                return True
            except socket.error:
                return False
            finally:
                sock.close()

        if check_internet_connection():
            battery_image4 = Image.open("Icones/WifiOn_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4
        else:
            battery_image4 = Image.open("Icones/WifiOff_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4

        # GPU button configuration
        battery_image5 = Image.open("Icones/GPU_icone.png")
        battery_image5 = battery_image5.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo5 = ImageTk.PhotoImage(battery_image5)

        gpu.config(text=f"GPU", justify=LEFT, anchor=W,
                   image=photo5, compound="left")

        Battery.image5 = photo5

        # Battery_Daytime_info
        today = date.today()
        d1 = today.strftime("%d/%m/%Y")
        day_week = calendar.day_name[today.weekday()][:3]

        time1 = dt.datetime.now().strftime('%H:%M:%S')

        def secs2hours(secs):
            mm, ss = divmod(secs, 60)
            hh, mm = divmod(mm, 60)
            return "%d h %02d min %02d s" % (abs(hh), mm, ss)

        battery = psutil.sensors_battery()
        percentage = battery.percent
        time_bat = battery.secsleft

        battery_image = Image.open(f"Batterie/Batterie_Pourcentage_{percentage}.png")
        battery_image = battery_image.resize((50, 35),
                                             Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo = ImageTk.PhotoImage(battery_image)

        AC_power = psutil.sensors_battery()[2]
        status = "⌁ Charging    "
        if AC_power is False or None:
            status = "Not Charging"

        Battery.configure(
            text=f"Time Left: {secs2hours(time_bat)}            {day_week} {d1}\nStatus: {status}             "
                 f"                    {time1}",
            image=photo, compound="left", anchor=E)
        Battery.image = photo

        #Info1
        used_space = round(psutil.disk_usage("C:\\")[1] / 1024**3, 1)
        total_space = round(psutil.disk_usage("C:\\")[0] / 1024**3, 1)
        percentage_space = psutil.disk_usage("C:\\")[3]
        free_space = round(psutil.disk_usage("C:\\")[2] / 1024**3, 1)

        Disk_used.config(text=f"{used_space} GB")
        Disk_total.config(text=f"{total_space} GB")
        Disk_percent.config(text=f"{percentage_space} %")
        Disk_free.config(text=f"{free_space} GB")


        #Plot

        percent_disk = psutil.disk_usage("C:\\")[3]
        y.append(percent_disk)

        read_bytes = psutil.disk_io_counters()[2]
        time.sleep(1)
        read_bytes1 = psutil.disk_io_counters()[2]
        r_rate = round((read_bytes1-read_bytes) / 1024**2, 1)
        y1.append(r_rate)

        write_bytes = psutil.disk_io_counters()[3]
        time.sleep(1)
        write_bytes1 = psutil.disk_io_counters()[3]
        w_rate = round((write_bytes1-write_bytes) / 1024**2, 1)
        y2.append(w_rate)


        read_speed = r_rate
        read_count = psutil.disk_io_counters()[0]
        read_time = round(psutil.disk_io_counters()[4] / 10**3)
        write_speed = w_rate
        write_count = psutil.disk_io_counters()[1]
        write_time = round(psutil.disk_io_counters()[5] / 10**3)

        disk_read_speed.config(text=f"{read_speed} MB/s")
        disk_read_count.config(text=f"{read_count}")
        disk_read_time.config(text=f"{read_time} s")
        disk_write_speed.config(text=f"{write_speed} MB/s")
        disk_write_count.config(text=f"{write_count}")
        disk_write_time.config(text=f"{write_time} s")

        read_avr = round(sum(y1) / len(y1), 1)
        write_avr = round(sum(y2) / len(y2), 1)
        read_average.config(text=f"{read_avr} MB/s")
        write_average.config(text=f"{write_avr} MB/s")


        x = x[-61:]
        y = y[-61:]
        x = x[-len(y):]
        y = y[-len(y):]

        x1 = x1[-61:]
        y1 = y1[-61:]
        x1 = x1[-len(y):]
        y1 = y1[-len(y):]

        x2 = x2[-61:]
        y2 = y2[-61:]
        x2 = x2[-len(y):]
        y2 = y2[-len(y):]

        ax.clear()
        ax1.clear()

        ax.plot(x, y, color="green")
        ax.fill_between(x, y, 0, color='green', alpha=.1)

        ax1.plot(x1, y1, color="green")
        ax1.fill_between(x1, y1, 0, color='green', alpha=.1)
        ax1.plot(x2, y2, color="#006400", linestyle = 'dashed')
        ax1.fill_between(x2, y2, 0, color='#006400', alpha=.1)

        ax.set_ylim(0, 101)
        ax.set_xlim(60,0)

        ax1.set_ylim(0, 10)
        ax1.set_xlim(60,0)

        ax.spines["bottom"].set_color("white")
        ax.spines["left"].set_color("white")
        ax.tick_params(axis="x", colors="white")
        ax.tick_params(axis="y", colors="white")
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.title.set_color('white')
        ax.set_facecolor("black")

        plt.subplots_adjust(bottom=0.10)
        ax.set_xlabel('Time period, s')
        ax.set_ylabel('Disk Usage, %')

        ax.grid(color='c', linewidth=0.2)

        ax1.spines["bottom"].set_color("white")
        ax1.spines["left"].set_color("white")
        ax1.tick_params(axis="x", colors="white")
        ax1.tick_params(axis="y", colors="white")
        ax1.xaxis.label.set_color('white')
        ax1.yaxis.label.set_color('white')
        ax1.title.set_color('white')
        ax1.set_facecolor("black")


        plt.subplots_adjust(bottom=0.10)
        ax1.set_xlabel('Time period, s')
        ax1.set_ylabel('Disk Transfer Rate, MB/s')

        ax1.grid(color='c', linewidth=0.2)

    x = [i for i in range(60, -1, -1)]
    y = []

    x1 = [i for i in range(60, -1, -1)]
    y1 = []

    x2 = [i for i in range(60, -1, -1)]
    y2 = []

    fig = plt.figure(figsize=(11, 6), facecolor='black')
    fig.subplots_adjust(left=0.075, bottom=0.07, right=0.95, top=0.95, wspace=0, hspace=0)
    ax = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(3, 1, 3)
    ax.set_facecolor('black')
    ax1.set_facecolor('black')

    ani = FuncAnimation(fig, my_function, fargs=(x, y, x1, y1, x2, y2), interval=1000)
    canvas = FigureCanvasTkAgg(fig, master=initial_screen)
    canvas.get_tk_widget().grid(row=3, column=1, columnspan=3)


def plot_network():
    """
    Procédure pour tracer de la vitesse de l'envoi et de la récéption de données
    sur un réseau (en Kbps) en fonction du temps.

    Modifie également les valeurs écrites dans les Labels pour différentes données statistiques.

    Pourcentage batterie, date et heure sont également modifiées à itération de la procédure update_plot()
    au sein de l'animation Animate Func du module Matplotlib.

    Plusieurs variables de portée globale utilisées et dont les valeurs sont modifiées en permanence
    lors de l'appel de la procédure.
    """
    global ani, disk, cpu, percent_cpu, memory, disk, Battery, disk, network, gpu, menu
    global send_bitrate, receive_bitrate, ipv4, ipv6, subnet_mask
    global bytes_sent, packets_sent, errin, dropin, bytes_received, packets_received, errout, dropout
    global sent_average, receive_average

    def my_function(i, x1, y1, x2, y2):
        """
        Fonction qui permet de tracer le graphique du disque C

        Paramètres:
        x1 (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y1 (list): valeur mesurée pour la vitesse d'envoi des données sur le réseau (61 valeurs max, même principe
         de 'update' que la liste x.
        x2 (list): le temps en seconde (sur 1 min, soit 61 valeurs max, qui se remplit
         selon le principe FILO).
        y2 (list): valeur mesurée pour la vitesse de la réception des données sur le réseau (61 valeurs max,
         même principe de 'update' que la liste x.
        """
        # General
        percent_cpu = psutil.cpu_percent(1)
        freq = round(psutil.cpu_freq()[0] / 10 ** 3, 2)

        battery_image1 = Image.open("Icones/CPU_icone.png")
        battery_image1 = battery_image1.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo1 = ImageTk.PhotoImage(battery_image1)

        cpu.config(text=f"CPU\n{percent_cpu} % {freq} GHz", justify=LEFT, anchor=W, image=photo1,
                   compound="left")

        Battery.image1 = photo1

        free_memory = round(psutil.virtual_memory()[3] / 10 ** 9, 1)
        total_memory = round(psutil.virtual_memory()[0] / 10 ** 9, 1)
        percent_memory = psutil.virtual_memory()[2]

        battery_image2 = Image.open("Icones/Ram_icone.png")
        battery_image2 = battery_image2.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo2 = ImageTk.PhotoImage(battery_image2)

        memory.config(text=f"Memory\n{free_memory} / {total_memory} GB ({percent_memory} %)",
                      justify=LEFT, anchor=W, image=photo2, compound="left")

        Battery.image2 = photo2

        disk_load = psutil.disk_usage("C:\\")[3]

        battery_image3 = Image.open("Icones/Disk_icone.png")
        battery_image3 = battery_image3.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo3 = ImageTk.PhotoImage(battery_image3)

        disk.config(text=f"Disk 0 (C:)\n{disk_load} %", justify=LEFT, anchor=W,
                    image=photo3, compound="left")

        Battery.image3 = photo3

        # Wi-Fi button configuration
        recv_bytes = psutil.net_io_counters().bytes_recv
        sent_bytes = psutil.net_io_counters().bytes_sent
        time.sleep(1)
        recv_bytes1 = psutil.net_io_counters().bytes_recv
        sent_bytes1 = psutil.net_io_counters().bytes_sent
        recv_rate = (recv_bytes1 - recv_bytes) * 0.008
        y1.append(recv_rate)
        sent_rate = (sent_bytes1 - sent_bytes) * 0.008
        y2.append(sent_rate)

        def check_internet_connection():
            remote_server = "www.google.com"
            port = 80
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            try:
                sock.connect((remote_server, port))
                return True
            except socket.error:
                return False
            finally:
                sock.close()

        if check_internet_connection():
            battery_image4 = Image.open("Icones/WifiOn_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4
        else:
            battery_image4 = Image.open("Icones/WifiOff_icone.png")
            battery_image4 = battery_image4.resize((110, 90),
                                                   Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
            photo4 = ImageTk.PhotoImage(battery_image4)
            network.config(text=f"Network\nS: {round(sent_rate, 1)} Kbps\nR: {round(recv_rate, 1)} Kbps",
                           justify=LEFT, anchor=W, image=photo4, compound="left")
            Battery.image4 = photo4

        # GPU button configuration
        battery_image5 = Image.open("Icones/GPU_icone.png")
        battery_image5 = battery_image5.resize((110, 90),
                                               Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo5 = ImageTk.PhotoImage(battery_image5)

        gpu.config(text=f"GPU", justify=LEFT, anchor=W,
                   image=photo5, compound="left")

        Battery.image5 = photo5

        # Battery_Daytime_info
        today = date.today()
        d1 = today.strftime("%d/%m/%Y")
        day_week = calendar.day_name[today.weekday()][:3]

        time1 = dt.datetime.now().strftime('%H:%M:%S')

        def secs2hours(secs):
            mm, ss = divmod(secs, 60)
            hh, mm = divmod(mm, 60)
            return "%d h %02d min %02d s" % (abs(hh), mm, ss)

        battery = psutil.sensors_battery()
        percentage = battery.percent
        time_bat = battery.secsleft

        battery_image = Image.open(f"Batterie/Batterie_Pourcentage_{percentage}.png")
        battery_image = battery_image.resize((50, 35),
                                             Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
        photo = ImageTk.PhotoImage(battery_image)

        AC_power = psutil.sensors_battery()[2]
        status = "⌁ Charging    "
        if AC_power is False or None:
            status = "Not Charging"

        Battery.configure(
            text=f"Time Left: {secs2hours(time_bat)}            {day_week} {d1}\nStatus: {status}             "
                 f"                    {time1}", image=photo, compound="left", anchor=E)
        Battery.image = photo

        send_bits = sent_rate
        received_bits = recv_rate

        #Tester si l'on peut récupérer l'adresse IPv4 du PC
        try:
            hostname = socket.gethostname()
            ipv4_address = socket.gethostbyname(hostname)
        except socket.gaierror:
            ipv4_address = "Unavailable"
        except Exception as e:
            ipv4_address = "Unavailable"

        #Tester si l'on peut récupérer l'adresse IPv6 du PC
        try:
            hostname = socket.gethostname()
            ipv6_address = socket.getaddrinfo(hostname, None, socket.AF_INET6)
        except socket.gaierror:
            ipv6_address = "Unavailable"
        except Exception as e:
            ipv6_address = "Unavailable"

        send_bitrate.config(text=f"{round(send_bits, 1)} Kbps")
        receive_bitrate.config(text=f"{round(received_bits, 1)} Kbps")
        ipv4.config(text=f"{ipv4_address}")
        ipv6.config(text=f"{ipv6_address[0][4][0]}")

        sent_bytes = round(psutil.net_io_counters()[0] / 1024**2, 1) #in MB
        sent_packets = psutil.net_io_counters()[2]
        errors_in = psutil.net_io_counters()[4]
        dropped_in = psutil.net_io_counters()[6]

        received_bytes = round(psutil.net_io_counters()[1] / 1024**2, 1)  # in MB
        received_packets = psutil.net_io_counters()[3]
        errors_out = psutil.net_io_counters()[5]
        dropped_out = psutil.net_io_counters()[7]

        bytes_sent.config(text=f"{sent_bytes} KB")
        packets_sent.config(text=f"{sent_packets}")
        errin.config(text=f"{errors_in}")
        dropin.config(text=f"{dropped_in}")

        bytes_received.config(text=f"{received_bytes} KB")
        packets_received.config(text=f"{received_packets}")
        errout.config(text=f"{errors_out}")
        dropout.config(text=f"{dropped_out}")

        average_sent = round(sum(y1) / len(y1), 1)
        average_received = round(sum(y2) / len(y2), 1)

        sent_average.config(text=f"{average_sent} Kbps")
        receive_average.config(text=f"{average_received} Kbps")

        addrs = psutil.net_if_addrs()

        #Tester si l'on peut récuper le masque sous-réseau
        try:
            subnet = addrs['Connexion au réseau local* 2'][1][2]
        except:
            subnet = "Unavailable"
        subnet_mask.config(text=f"{subnet}")


        x1 = x1[-61:]
        y1 = y1[-61:]
        x1 = x1[-len(y1):]
        y1 = y1[-len(y1):]

        x2 = x2[-61:]
        y2 = y2[-61:]
        x2 = x2[-len(y2):]
        y2 = y2[-len(y2):]

        ax.clear()

        ax.plot(x1, y1, color="orange", linestyle = 'dashed')
        ax.fill_between(x1, y1, 0, color='orange', alpha=.1)
        ax.plot(x2, y2, color="red")
        ax.fill_between(x2, y2, 0, color='red', alpha=.1)

        ax.set_ylim(0, 101)
        ax.set_xlim(60, 0)

        ax.spines["bottom"].set_color("white")
        ax.spines["left"].set_color("white")
        ax.tick_params(axis="x", colors="white")
        ax.tick_params(axis="y", colors="white")
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.title.set_color('white')
        ax.set_facecolor("black")

        plt.subplots_adjust(bottom=0.10)
        ax.set_xlabel('Time period, s')
        ax.set_ylabel('Network Transfer Rate, Kbps')

        ax.grid(color='c', linewidth=0.2)

    x1 = [i for i in range(60, -1, -1)]
    y1 = []

    x2 = [i for i in range(60, -1, -1)]
    y2 = []

    fig, ax = plt.subplots(figsize=(11, 6), facecolor='black')
    fig.subplots_adjust(left=0.075, bottom=0.07, right=0.95, top=0.95, wspace=0, hspace=0)

    ani = FuncAnimation(fig, my_function, fargs=(x1,y1,x2,y2), interval=200)
    canvas = FigureCanvasTkAgg(fig, master=initial_screen)
    canvas.get_tk_widget().grid(row=3, column=1, columnspan=4)


def cpu_statistics():
    """
    Procédure qui permet de configurer la page avec les informations relatives au CPU.
    Ici est défini et placé dans le grid l'ensemble des widgets de cette page.

    Ulitisation des variables globales pour chaque widget car certaines nécessitent d'être traitées
    au sein des fonctions update_plot() des procédures pour tracer les graphiques en temps réel,

    d'où la nécessité d'avoir une portée globale.
    """
    global anim, percent_cpu, cpu, memory, Timer, disk, Battery, network, gpu
    global CPU_realtime_txt, CPU_realtime, CPU_speed_txt, CPU_speed, CPU_processes_txt, CPU_processes
    global CPU_threads_txt, CPU_threads, CPU_handles_txt, CPU_handles
    global cores_txt, cores
    global num_switches, num_interrupts, num_syscalls
    global cpu_average, CPU_uptime


    for widget in initial_screen.winfo_children():
        widget.destroy()

    main_menu = tk.Frame(initial_screen, bg="grey")
    main_menu.rowconfigure(1)
    main_menu.columnconfigure(7)
    main_menu.grid(row=0, column = 0, columnspan = 7, sticky = NW)

    processes = tk.Button(main_menu, text="Processes", fg = "white", bg = "black", command=lambda: new_funct())
    processes.grid(row=0, column=0)

    performance = tk.Button(main_menu, text="Performance", command = lambda: cpu_statistics(),
                            fg = "white", bg = "black")
    performance.grid(row=0, column=1)

    app_history = tk.Button(main_menu, text="App history", fg = "white", bg = "black", command=lambda: new_funct())
    app_history.grid(row=0, column=2)

    startup = tk.Button(main_menu, text="Startup", fg = "white", bg = "black", command=lambda: new_funct())
    startup.grid(row=0, column=3)

    users = tk.Button(main_menu, text="Users", fg = "white", bg = "black", command=lambda: new_funct())
    users.grid(row=0, column=4)

    details = tk.Button(main_menu, text="Details", fg = "white", bg = "black", command=lambda: new_funct())
    details.grid(row=0, column=5)

    services = tk.Button(main_menu, text="Services", fg = "white", bg = "black", command=lambda: new_funct())
    services.grid(row=0, column=6)


    menu = tk.Frame(initial_screen, bg="grey")
    menu.rowconfigure(5)
    menu.columnconfigure(1)
    menu.grid(row = 2, column = 0, sticky = NW, rowspan = 5, pady = 20)


    cpu = tk.Button(menu, text=f"CPU \n {percent_cpu}", font=("", "15"), anchor=W, width=315, height=90,
                    command = lambda: [threads(), cpu_statistics()], fg = "white", bg = "black")
    cpu.grid(row=0, column=0)

    memory = tk.Button(menu, text="Memory", font=("", "15"), anchor=W, width=315, height=90,
                       command = lambda: memory_statistics(), fg = "white", bg = "black")
    memory.grid(row=1, column=0)

    disk = tk.Button(menu, text="Disk", font=("", "15"), anchor=W, width=315, height=90,
                     fg = "white", bg = "black", command=lambda: disk_statistics())
    disk.grid(row=2, column=0)

    network = tk.Button(menu, text="Wi-Fi", font=("", "15"), anchor=W, width=315, height=90,
                        fg = "white", bg = "black", command=lambda: network_statistics())
    network.grid(row=3, column=0)

    gpu = tk.Button(menu, text="GPU", font=("", "15"), anchor=W, width=315, height=90,
                        fg="white", bg="black", command=lambda: yet_to_come())
    gpu.grid(row=4, column=0)


    cpu_label = tk.Label(initial_screen, text = "CPU",
                         font = ("", "25"), anchor = W, fg = "white", bg = "black", width = 7)
    cpu_label.grid(row = 2, column = 1, sticky = NW, padx = 10, pady = 10)


    processor_name = tk.Label(initial_screen, text = f"{cpuinfo.get_cpu_info()['brand_raw']}",
                              font = ("", "15"), anchor = E, fg = "white", bg = "black", width = 50)
    processor_name.grid(row= 2, column = 3, sticky = SE, pady = 10)

    plot_cpu()

    #STATISTICS

    #1. Current percentage
    CPU_realtime_txt = tk.Label(initial_screen, text="Percentage", anchor=W,
                            height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_realtime_txt.grid(row = 4, column = 1, sticky=NW, padx=20)

    CPU_realtime = tk.Label(initial_screen, text="", anchor=W,
                            height=2, width=10, bg="black", fg="white", font=("", 20))
    CPU_realtime.grid(row=5, column=1, sticky=NW, padx=20)

    #2. Speed
    CPU_speed_txt = tk.Label(initial_screen, text="Speed", anchor=W,
                         height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_speed_txt.grid(row=4, column=2, sticky=NW)

    CPU_speed = tk.Label(initial_screen, text="", anchor=W,
                             height=2, width=10, bg="black", fg="white", font=("", 20))
    CPU_speed.grid(row=5, column=2, sticky=NW)

    #2bis. Uptime
    CPU_uptime_txt = tk.Label(initial_screen, text="Uptime", anchor=W,
                             height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_uptime_txt.grid(row=4, column=3, sticky=NW)

    CPU_uptime = tk.Label(initial_screen, text="", anchor=W,
                         height=2, width=10, bg="black", fg="white", font=("", 20))
    CPU_uptime.grid(row=5, column=3, sticky=NW)

    #3. Threads
    CPU_threads_txt = tk.Label(initial_screen, text="Threads", anchor=W,
                           height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_threads_txt.grid(row=6, column = 1, sticky=NW, padx=20)
    CPU_threads = tk.Label(initial_screen, text="", anchor=W,
                               height=1, width=10, bg="black", fg="white", font=("", 20))
    CPU_threads.grid(row=7, column=1, sticky=NW, padx=20)


    #4. Total processes
    CPU_processes_txt = tk.Label(initial_screen, text="Processes", anchor=W,
                           height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_processes_txt.grid(row=6, column = 2, sticky=NW)
    CPU_processes = tk.Label(initial_screen, text="", anchor=W,
                                 height=1, width=10, bg="black", fg="white", font=("", 20))
    CPU_processes.grid(row=7, column=2, sticky=NW)

    #5. Total threads
    CPU_handles_txt = tk.Label(initial_screen, text="Handles", anchor=W,
                           height=1, width=10, bg="black", fg="grey", font=("", 15))
    CPU_handles_txt.grid(row=6, column = 3, sticky=NW)
    CPU_handles = tk.Label(initial_screen, text="", anchor=W,
                               height=1, width=10, bg="black", fg="white", font=("", 20))
    CPU_handles.grid(row=7, column=3, sticky=NW)

    #PERFORMANCE PER CPU
    CPU_cores = tk.Frame(initial_screen, bg="black")
    CPU_cores.rowconfigure(2 + psutil.cpu_count())
    CPU_cores.columnconfigure(2)
    CPU_cores.grid(row=3, column = 4, columnspan=2, sticky=N, padx=30)

    #Heading
    name_cores = tk.Label(CPU_cores, text = "Performance per CPU",
                       height = 1, width = 20, bg = "black", fg = "grey", font=("", 12))
    name_cores.grid(row=1, column=1, columnspan=2, pady=10)

    #1.Cores
    num_cores_txt = tk.Label(CPU_cores, text = f"Cores ", anchor=W,
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 15))
    num_cores_txt.grid(row=2, column=1, sticky=NW)

    num_cores = tk.Label(CPU_cores, text=f"{psutil.cpu_count()}", anchor=E,
                             height=1, width=10, bg="black", fg="white", font=("", 15))
    num_cores.grid(row=2, column=2, sticky=NW)

    #2.Logical cores
    num_log_cores_txt = tk.Label(CPU_cores, text=f"Logical cores ", anchor = W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    num_log_cores_txt.grid(row=3, column=1, sticky=NW)

    num_log_cores = tk.Label(CPU_cores, text=f"{psutil.cpu_count(logical=True)}", anchor=E,
                         height=1, width=10, bg="black", fg="white", font=("", 15))
    num_log_cores.grid(row=3, column=2, sticky=NW)

    blank = tk.Label(CPU_cores, text="", anchor=E,
                             height=1, width=10, bg="black", fg="white", font=("", 15))
    blank.grid(row=4, column=1, columnspan=2)

    Trivia_CPU = tk.Frame(initial_screen, bg="black")
    Trivia_CPU.rowconfigure(4)
    Trivia_CPU.columnconfigure(2)
    Trivia_CPU.grid(row=4, column = 4, rowspan=5, columnspan=2, sticky=N, padx=30, pady = 30)

    # Heading
    cpu_stats = tk.Label(Trivia_CPU, text="Statistics",
                          height=1, width=20, bg="black", fg="grey", font=("", 12))
    cpu_stats.grid(row=1, column=1, columnspan=2, pady=10)

    # 1.Switches
    num_switches_txt = tk.Label(Trivia_CPU, text=f"Context switches: ", anchor=W,
                             height=1, width=30, bg="black", fg="white", font=("", 15))
    num_switches_txt.grid(row=2, column=1, sticky=NW)

    num_switches = tk.Label(Trivia_CPU, text=f"", anchor=E,
                         height=1, width=10, bg="black", fg="white", font=("", 15))
    num_switches.grid(row=2, column=2, sticky=NW)

    # 2.Interrupts
    num_interrupts_txt = tk.Label(Trivia_CPU, text=f"Interrupts: ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    num_interrupts_txt.grid(row=3, column=1, sticky=NW)

    num_interrupts = tk.Label(Trivia_CPU, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    num_interrupts.grid(row=3, column=2, sticky=NW)

    # 3.System calls
    num_syscalls_txt = tk.Label(Trivia_CPU, text=f"System calls: ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    num_syscalls_txt.grid(row=4, column=1, sticky=NW)

    num_syscalls = tk.Label(Trivia_CPU, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    num_syscalls.grid(row=4, column=2, sticky=NW)

    #4.CPU Average
    cpu_average_txt = tk.Label(Trivia_CPU, text=f"Utilization average (per min): ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    cpu_average_txt.grid(row=5, column=1, sticky=NW)

    cpu_average = tk.Label(Trivia_CPU, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    cpu_average.grid(row=5, column=2, sticky=NW)


    #3. Percent utilization per core

    cores_txt = []
    cores = []
    for i in range(psutil.cpu_count()):
        core_txt = tk.Label(CPU_cores, text=f"Core {i+1}", anchor=W,
                         height=1, width=20, bg="black", fg="white", font=("", 15))
        cores_txt.append(core_txt)
        core_txt.grid(row=5 + i, column=1, sticky=NW)
        core = tk.Label(CPU_cores, text=f"{psutil.cpu_percent(interval=1, percpu=True)[i]}", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
        cores.append(core)
        core.grid(row=5+i, column=2, sticky=NW)


    Battery = tk.Label(initial_screen, text="", fg = "white", bg = "black", anchor=E)
    Battery.grid(row=0, column=4, rowspan=2, columnspan=2, sticky=E, padx = 5)

    #CPU_Utilization = tk.Label(initial_screen, text = "", fg="white", bg="black")


def memory_statistics():
    """
    Procédure qui permet de configurer la page avec les informations relatives à la RAM.
    Ici est défini et placé dans le grid l'ensemble des widgets de cette page.

    Ulitisation des variables globales pour chaque widget car certaines nécessitent d'être traitées
    au sein des fonctions update_plot() des procédures pour tracer les graphiques en temps réel,

    d'où la nécessité d'avoir une portée globale.
    """
    global anim1, percent_cpu, cpu, memory, disk, Battery, network, gpu, menu
    global in_use, available, percent_ram, total, free
    global total_swap, swap_used, swap_percent, swap_free, swap_sin, swap_sout
    global average_percent

    for widget in initial_screen.winfo_children():
        widget.destroy()

    main_menu = tk.Frame(initial_screen, bg="grey")
    main_menu.rowconfigure(1)
    main_menu.columnconfigure(7)
    main_menu.grid(row=0, column = 0, columnspan = 7, sticky = NW)

    processes = tk.Button(main_menu, text="Processes", fg = "white", bg = "black", command=lambda: new_funct())
    processes.grid(row=0, column=0)

    performance = tk.Button(main_menu, text="Performance", command=lambda: cpu_statistics(),
                            fg = "white", bg = "black")
    performance.grid(row=0, column=1)

    app_history = tk.Button(main_menu, text="App history", fg = "white", bg = "black", command=lambda: new_funct())
    app_history.grid(row=0, column=2)

    startup = tk.Button(main_menu, text="Startup", fg = "white", bg = "black", command=lambda: new_funct())
    startup.grid(row=0, column=3)

    users = tk.Button(main_menu, text="Users", fg = "white", bg = "black", command=lambda: new_funct())
    users.grid(row=0, column=4)

    details = tk.Button(main_menu, text="Details", fg = "white", bg = "black", command=lambda: new_funct())
    details.grid(row=0, column=5)

    services = tk.Button(main_menu, text="Services", fg = "white", bg = "black", command=lambda: new_funct())
    services.grid(row=0, column=6)


    menu = tk.Frame(initial_screen, bg="grey")
    menu.rowconfigure(5)
    menu.columnconfigure(1)
    menu.grid(row=2, column=0, sticky=NW, rowspan=5, pady=20)

    cpu = tk.Button(menu, text=f"CPU \n {percent_cpu}", font=("", "15"), anchor=W, width=315, height=90,
                    command=lambda: cpu_statistics(), fg = "white", bg = "black")
    cpu.grid(row=0, column=0)

    memory = tk.Button(menu, text="Memory", font=("", "15"), anchor=W, width=315, height=90,
                       command = lambda: memory_statistics(), fg = "white", bg = "black")
    memory.grid(row=1, column=0)

    disk = tk.Button(menu, text="Disk", font=("", "15"), anchor=W, width=315, height=90,
                     fg = "white", bg = "black", command=lambda: disk_statistics())
    disk.grid(row=2, column=0)

    network = tk.Button(menu, text="Wi-Fi", font=("", "15"), anchor=W, width=315, height=90,
                        fg = "white", bg = "black", command=lambda: network_statistics())
    network.grid(row=3, column=0)

    gpu = tk.Button(menu, text="GPU", font=("", "15"), anchor=W, width=315, height=90,
                        fg="white", bg="black", command=lambda: yet_to_come())
    gpu.grid(row=4, column=0)


    memory_label = tk.Label(initial_screen, text="Memory", font=("", "25"), anchor=W,
                         fg="white", bg="black")
    memory_label.grid(row = 2, column = 1, sticky = NW, padx = 10, pady = 10)

    plot_memory()
    """
    Timer = tk.Label(initial_screen, text="")
    Timer.grid(row=0, column=2, sticky=NE)
    """
    Battery = tk.Label(initial_screen, text="", fg="white", bg="black", anchor=E)
    Battery.grid(row=0, column=4, rowspan=2, columnspan=2, sticky=E, padx=5)

    # STATISTICS

    # 1. In use memory
    in_use_txt = tk.Label(initial_screen, text="In Use", anchor=W,
                                height=1, width=10, bg="black", fg="grey", font=("", 15))
    in_use_txt.grid(row=4, column=1, sticky=NW, padx=20)

    in_use = tk.Label(initial_screen, text="", anchor=W,
                            height=2, width=10, bg="black", fg="white", font=("", 20))
    in_use.grid(row=5, column=1, sticky=NW, padx=20)

    # 2. Available
    available_txt = tk.Label(initial_screen, text="Available", anchor=W,
                             height=1, width=10, bg="black", fg="grey", font=("", 15))
    available_txt.grid(row=4, column=2, sticky=NW)

    available = tk.Label(initial_screen, text="", anchor=W,
                         height=2, width=10, bg="black", fg="white", font=("", 20))
    available.grid(row=5, column=2, sticky=NW)

    # 3. Percent
    percent_ram_txt = tk.Label(initial_screen, text="Percent Used", anchor=W,
                               height=1, width=11, bg="black", fg="grey", font=("", 15))
    percent_ram_txt.grid(row=6, column=1, sticky=NW, padx=20)
    percent_ram = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=10, bg="black", fg="white", font=("", 20))
    percent_ram.grid(row=7, column=1, sticky=NW, padx=20)

    # 4. Total
    total_txt = tk.Label(initial_screen, text="Total", anchor=W,
                                 height=1, width=10, bg="black", fg="grey", font=("", 15))
    total_txt.grid(row=6, column=2, sticky=NW)
    total = tk.Label(initial_screen, text="", anchor=W,
                             height=1, width=10, bg="black", fg="white", font=("", 20))
    total.grid(row=7, column=2, sticky=NW)

    # 5. Free
    free_txt = tk.Label(initial_screen, text="Free", anchor=W,
                               height=1, width=10, bg="black", fg="grey", font=("", 15))
    free_txt.grid(row=6, column=3, sticky=NW)
    free = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=10, bg="black", fg="white", font=("", 20))
    free.grid(row=7, column=3, sticky=NW)

    #TRIVIA
    Trivia_RAM = tk.Frame(initial_screen, bg="black")
    Trivia_RAM.rowconfigure(7)
    Trivia_RAM.columnconfigure(2)
    Trivia_RAM.grid(row=3, column=4, columnspan=2, sticky=N, padx=30)

    # Heading
    ram_heading = tk.Label(Trivia_RAM, text="Swap Memory Check",
                          height=1, width=20, bg="black", fg="grey", font=("", 12))
    ram_heading.grid(row=1, column=1, columnspan=2, pady=10)

    # 1.Total
    total_swap_txt = tk.Label(Trivia_RAM, text=f"Total ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    total_swap_txt.grid(row=2, column=1, sticky=NW)

    total_swap = tk.Label(Trivia_RAM, text="", anchor=E,
                         height=1, width=15, bg="black", fg="white", font=("", 15))
    total_swap.grid(row=2, column=2, sticky=NW)

    # 2.Used
    swap_used_txt = tk.Label(Trivia_RAM, text=f"Used ", anchor=W,
                                 height=1, width=20, bg="black", fg="white", font=("", 15))
    swap_used_txt.grid(row=3, column=1, sticky=NW)

    swap_used = tk.Label(Trivia_RAM, text="", anchor=E,
                             height=1, width=15, bg="black", fg="white", font=("", 15))
    swap_used.grid(row=3, column=2, sticky=NW)

    # 3.Percent_used
    swap_percent_txt = tk.Label(Trivia_RAM, text=f"Percentage Usage ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    swap_percent_txt.grid(row=4, column=1, sticky=NW)

    swap_percent = tk.Label(Trivia_RAM, text="", anchor=E,
                         height=1, width=15, bg="black", fg="white", font=("", 15))
    swap_percent.grid(row=4, column=2, sticky=NW)

    # 4.Free
    swap_free_txt = tk.Label(Trivia_RAM, text=f"Free ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    swap_free_txt.grid(row=5, column=1, sticky=NW)

    swap_free = tk.Label(Trivia_RAM, text="", anchor=E,
                         height=1, width=15, bg="black", fg="white", font=("", 15))
    swap_free.grid(row=5, column=2, sticky=NW)

    # 5.Sin (bytes)
    swap_sin_txt = tk.Label(Trivia_RAM, text=f"Swapped in ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    swap_sin_txt.grid(row=6, column=1, sticky=NW)

    swap_sin = tk.Label(Trivia_RAM, text="", anchor=E,
                         height=1, width=15, bg="black", fg="white", font=("", 15))
    swap_sin.grid(row=6, column=2, sticky=NW)

    # 6.Sout (bytes)
    swap_sout_txt = tk.Label(Trivia_RAM, text=f"Swapped out ", anchor=W,
                            height=1, width=20, bg="black", fg="white", font=("", 15))
    swap_sout_txt.grid(row=7, column=1, sticky=NW)

    swap_sout = tk.Label(Trivia_RAM, text="", anchor=E,
                        height=1, width=15, bg="black", fg="white", font=("", 15))
    swap_sout.grid(row=7, column=2, sticky=NW)

    #Average
    Average_stat = tk.Frame(initial_screen, bg="black")
    Average_stat.rowconfigure(2)
    Average_stat.columnconfigure(2)
    Average_stat.grid(row=5, column=4, rowspan = 3, columnspan=2, sticky=N, padx=30, pady=30)

    # Heading
    heading_stats = tk.Label(Average_stat, text="Average",
                         height=1, width=20, bg="black", fg="grey", font=("", 12))
    heading_stats.grid(row=1, column=1, columnspan=2, pady=10)

    # Percent
    average_percent_txt = tk.Label(Average_stat, text=f"Percent Usage (over 1 min): ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    average_percent_txt.grid(row=2, column=1, sticky=NW)

    average_percent = tk.Label(Average_stat, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    average_percent.grid(row=2, column=2, sticky=NW)



def disk_statistics():
    """
    Procédure qui permet de configurer la page avec les informations relatives au disque C.
    Ici est défini et placé dans le grid l'ensemble des widgets de cette page.

    Ulitisation des variables globales pour chaque widget car certaines nécessitent d'être traitées
    au sein des fonctions update_plot() des procédures pour tracer les graphiques en temps réel,

    d'où la nécessité d'avoir une portée globale.
    """
    global disk, cpu, percent_cpu, memory, disk, Battery, disk, network, gpu, menu
    global Disk_used, Disk_total, Disk_percent, Disk_free
    global disk_read_speed, disk_read_count, disk_read_time, disk_write_speed, disk_write_count, disk_write_time
    global read_average, write_average

    for widget in initial_screen.winfo_children():
        widget.destroy()

    main_menu = tk.Frame(initial_screen, bg="grey")
    main_menu.rowconfigure(1)
    main_menu.columnconfigure(7)
    main_menu.grid(row=0, column=0, columnspan=7, sticky=NW)

    processes = tk.Button(main_menu, text="Processes", fg = "white", bg = "black", command=lambda: new_funct())
    processes.grid(row=0, column=0)

    performance = tk.Button(main_menu, text="Performance", command=lambda: cpu_statistics(),
                            fg = "white", bg = "black")
    performance.grid(row=0, column=1)

    app_history = tk.Button(main_menu, text="App history", fg = "white", bg = "black", command=lambda: new_funct())
    app_history.grid(row=0, column=2)

    startup = tk.Button(main_menu, text="Startup", fg = "white", bg = "black", command=lambda: new_funct())
    startup.grid(row=0, column=3)

    users = tk.Button(main_menu, text="Users", fg = "white", bg = "black", command=lambda: new_funct())
    users.grid(row=0, column=4)

    details = tk.Button(main_menu, text="Details", fg = "white", bg = "black", command=lambda: new_funct())
    details.grid(row=0, column=5)

    services = tk.Button(main_menu, text="Services", fg = "white", bg = "black", command=lambda: new_funct())
    services.grid(row=0, column=6)

    menu = tk.Frame(initial_screen, bg="grey")
    menu.rowconfigure(5)
    menu.columnconfigure(1)
    menu.grid(row=2, column=0, sticky=NW, rowspan=5, pady=20)

    cpu = tk.Button(menu, text=f"CPU \n {percent_cpu}", font=("", "15"), anchor=W, width=315, height=90,
                    command=lambda: cpu_statistics(), fg = "white", bg = "black")
    cpu.grid(row=0, column=0)

    memory = tk.Button(menu, text="Memory", font=("", "15"), anchor=W, width=315, height=90,
                       command = lambda: memory_statistics(), fg = "white", bg = "black")
    memory.grid(row=1, column=0)

    disk = tk.Button(menu, text="Disk", font=("", "15"), anchor=W, width=315, height=90,
                     fg = "white", bg = "black", command=lambda: disk_statistics())
    disk.grid(row=2, column=0)

    network = tk.Button(menu, text="Wi-Fi", font=("", "15"), anchor=W, width=315, height=90,
                        fg = "white", bg = "black", command=lambda: network_statistics())
    network.grid(row=3, column=0)

    gpu = tk.Button(menu, text="GPU", font=("", "15"), anchor=W, width=315, height=90,
                    fg = "white", bg = "black", command=lambda: yet_to_come())
    gpu.grid(row=4, column=0)

    disk_label = tk.Label(initial_screen, text="Disk", font=("", "25"), anchor=W,
                         fg="white", bg="black")
    disk_label.grid(row = 2, column = 1, sticky = NW, padx = 10, pady = 10)

    plot_disk()

    Battery = tk.Label(initial_screen, text="", fg="white", bg="black", anchor=E)
    Battery.grid(row=0, column=4, rowspan=2, sticky=E, padx=10)


    # STATISTICS_1

    #0. Blank row
    blank_row = tk.Label(initial_screen, text="", anchor=W,
                                height=1, width=10,  bg="black", fg="grey", font=("", 15))
    blank_row.grid(row=4, column=1, columnspan = 2, sticky=NW)


    # 1. Used space
    Disk_used_txt = tk.Label(initial_screen, text="Used space", anchor=W,
                                height=1, width=10, bg="black", fg="grey", font=("", 15))
    Disk_used_txt.grid(row=5, column=1, sticky=NW, padx=20)

    Disk_used = tk.Label(initial_screen, text="", anchor=W,
                            height=2, width=10, bg="black", fg="white", font=("", 20))
    Disk_used.grid(row=6, column=1, sticky=NW, padx=20)

    # 2. Total space
    Disk_total_txt = tk.Label(initial_screen, text="Total space", anchor=W,
                             height=1, width=10, bg="black", fg="grey", font=("", 15))
    Disk_total_txt.grid(row=5, column=2, sticky=NW)

    Disk_total = tk.Label(initial_screen, text="", anchor=W,
                         height=2, width=10, bg="black", fg="white", font=("", 20))
    Disk_total.grid(row=6, column=2, sticky=NW)

    # 3. Percentage used
    Disk_percent_txt = tk.Label(initial_screen, text="Percentage used", anchor=W,
                              height=1, width=15, bg="black", fg="grey", font=("", 15))
    Disk_percent_txt.grid(row=7, column=1, sticky=NW, padx=20)

    Disk_percent = tk.Label(initial_screen, text="", anchor=W,
                          height=2, width=10, bg="black", fg="white", font=("", 20))
    Disk_percent.grid(row=8, column=1, sticky=NW, padx=20)

    # 4. Free space
    Disk_free_txt = tk.Label(initial_screen, text="Free space", anchor=W,
                               height=1, width=10, bg="black", fg="grey", font=("", 15))
    Disk_free_txt.grid(row=7, column=2, sticky=NW)
    Disk_free = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=10, bg="black", fg="white", font=("", 20))
    Disk_free.grid(row=8, column=2, sticky=NW)


    #STATISTICS_2

    Disk_transfer = tk.Frame(initial_screen, bg="black")
    Disk_transfer.rowconfigure(8)
    Disk_transfer.columnconfigure(2)
    Disk_transfer.grid(row=3, column=4, columnspan=2, sticky=N, padx=30)

    # Heading
    disk_transfer_head = tk.Label(Disk_transfer, text="Disk Transfer Statistics",
                           height=1, width=20, bg="black", fg="grey", font=("", 12))
    disk_transfer_head.grid(row=1, column=1, columnspan=2, pady=10)

    # 1.Read_speed
    disk_read_speed_txt = tk.Label(Disk_transfer, text=f"Reading speed ", anchor=W,
                              height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_read_speed_txt.grid(row=2, column=1, sticky=NW)

    disk_read_speed = tk.Label(Disk_transfer, text="", anchor=E,
                          height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_read_speed.grid(row=2, column=2, sticky=NW)

    # 2.Read_count
    disk_read_count_txt = tk.Label(Disk_transfer, text=f"Number of reads ", anchor=W,
                                   height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_read_count_txt.grid(row=3, column=1, sticky=NW)

    disk_read_count = tk.Label(Disk_transfer, text="", anchor=E,
                               height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_read_count.grid(row=3, column=2, sticky=NW)

    # 3.Read_time
    disk_read_time_txt = tk.Label(Disk_transfer, text=f"Time spent reading ", anchor=W,
                                   height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_read_time_txt.grid(row=4, column=1, sticky=NW)

    disk_read_time = tk.Label(Disk_transfer, text="", anchor=E,
                               height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_read_time.grid(row=4, column=2, sticky=NW)

    # 4.Blank row
    blank_row1 = tk.Label(Disk_transfer, text="", anchor=E,
                              height=1, width=10, bg="black", fg="white", font=("", 15))
    blank_row1.grid(row=5, column=2, sticky=NW)

    # 5.Write_speed
    disk_write_speed_txt = tk.Label(Disk_transfer, text=f"Writing speed ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_write_speed_txt.grid(row=6, column=1, sticky=NW)

    disk_write_speed = tk.Label(Disk_transfer, text="", anchor=E,
                         height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_write_speed.grid(row=6, column=2, sticky=NW)

    # 6.Write count
    disk_write_count_txt = tk.Label(Disk_transfer, text=f"Number of writings ", anchor=W,
                             height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_write_count_txt.grid(row=7, column=1, sticky=NW)

    disk_write_count = tk.Label(Disk_transfer, text="", anchor=E,
                         height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_write_count.grid(row=7, column=2, sticky=NW)

    # 7.Write time
    disk_write_time_txt = tk.Label(Disk_transfer, text=f"Time spent writing ", anchor=W,
                                    height=1, width=20, bg="black", fg="white", font=("", 15))
    disk_write_time_txt.grid(row=8, column=1, sticky=NW)

    disk_write_time = tk.Label(Disk_transfer, text="", anchor=E,
                                height=1, width=10, bg="black", fg="white", font=("", 15))
    disk_write_time.grid(row=8, column=2, sticky=NW)

    # Average
    Disk_average = tk.Frame(initial_screen, bg="black")
    Disk_average.rowconfigure(3)
    Disk_average.columnconfigure(2)
    Disk_average.grid(row=6, column=4, rowspan=3, columnspan=2, sticky=N, padx=30, pady=30)

    # Heading
    heading_stats = tk.Label(Disk_average, text="Average Performance",
                             height=1, width=20, bg="black", fg="grey", font=("", 12))
    heading_stats.grid(row=1, column=1, columnspan=2, pady=10)

    # Read_average
    read_average_txt = tk.Label(Disk_average, text=f"Reading speed (over last min): ", anchor=W,
                                   height=1, width=30, bg="black", fg="white", font=("", 15))
    read_average_txt.grid(row=2, column=1, sticky=NW)

    read_average = tk.Label(Disk_average, text=f"", anchor=E,
                               height=1, width=10, bg="black", fg="white", font=("", 15))
    read_average.grid(row=2, column=2, sticky=NW)

    # Write_average
    write_average_txt = tk.Label(Disk_average, text=f"Writing speed (over last min): ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    write_average_txt.grid(row=3, column=1, sticky=NW)

    write_average = tk.Label(Disk_average, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    write_average.grid(row=3, column=2, sticky=NW)


def network_statistics():
    """
    Procédure qui permet de configurer la page avec les informations relatives au réseau.
    Ici est défini et placé dans le grid l'ensemble des widgets de cette page.

    Ulitisation des variables globales pour chaque widget car certaines nécessitent d'être traitées
    au sein des fonctions update_plot() des procédures pour tracer les graphiques en temps réel,

    d'où la nécessité d'avoir une portée globale.
    """
    global ani, disk, cpu, percent_cpu, memory, disk, Battery, disk, network, gpu, menu
    global send_bitrate, receive_bitrate, ipv4, ipv6, subnet_mask
    global bytes_sent, packets_sent, errin, dropin, bytes_received, packets_received, errout, dropout
    global sent_average, receive_average

    for widget in initial_screen.winfo_children():
        widget.destroy()

    main_menu = tk.Frame(initial_screen, bg="black")
    main_menu.rowconfigure(1)
    main_menu.columnconfigure(7)
    main_menu.grid(row=0, column=0, columnspan=7, sticky=NW)

    processes = tk.Button(main_menu, text="Processes", fg="white", bg="black", command=lambda: new_funct())
    processes.grid(row=0, column=0)

    performance = tk.Button(main_menu, text="Performance", command=lambda: cpu_statistics(),
                            fg="white", bg="black")
    performance.grid(row=0, column=1)

    app_history = tk.Button(main_menu, text="App history", fg="white", bg="black", command=lambda: new_funct())
    app_history.grid(row=0, column=2)

    startup = tk.Button(main_menu, text="Startup", fg="white", bg="black", command=lambda: new_funct())
    startup.grid(row=0, column=3)

    users = tk.Button(main_menu, text="Users", fg="white", bg="black", command=lambda: new_funct())
    users.grid(row=0, column=4)

    details = tk.Button(main_menu, text="Details", fg="white", bg="black", command=lambda: new_funct())
    details.grid(row=0, column=5)

    services = tk.Button(main_menu, text="Services", fg="white", bg="black", command=lambda: new_funct())
    services.grid(row=0, column=6)

    menu = tk.Frame(initial_screen, bg="grey")
    menu.rowconfigure(5)
    menu.columnconfigure(1)
    menu.grid(row=2, column=0, sticky=NW, rowspan=5, pady=20)

    cpu = tk.Button(menu, text=f"CPU \n {percent_cpu}", font=("", "15"), anchor=W, width=315, height=90,
                    command=lambda: cpu_statistics(), fg="white", bg="black")
    cpu.grid(row=0, column=0)

    memory = tk.Button(menu, text="Memory", font=("", "15"), anchor=W, width=315, height=90,
                       command=lambda: memory_statistics(), fg="white", bg="black")
    memory.grid(row=1, column=0)

    disk = tk.Button(menu, text="Disk", font=("", "15"), anchor=W, width=315, height=90,
                     fg="white", bg="black", command=lambda: disk_statistics())
    disk.grid(row=2, column=0)

    network = tk.Button(menu, text="Wi-Fi", font=("", "15"), anchor=W, width=315, height=90,
                        fg="white", bg="black", command=lambda: network_statistics())
    network.grid(row=3, column=0)

    gpu = tk.Button(menu, text="GPU", font=("", "15"), anchor=W, width=315, height=90,
                    fg="white", bg="black", command=lambda: yet_to_come())
    gpu.grid(row=4, column=0)

    network_label = tk.Label(initial_screen, text="Network", font=("", "25"), anchor=W,
                          fg="white", bg="black")
    network_label.grid(row=2, column=1, sticky=NW, padx=10, pady=10)

    Battery = tk.Label(initial_screen, text="", fg="white", bg="black", anchor=E)
    Battery.grid(row=0, column=5, rowspan=2, columnspan=2, padx=5)

    plot_network()

    # STATISTICS

    # 1. Send bitrate

    bitrate_image = Image.open(f"additional_images/Lgne2Rouge.png")
    bitrate_image = bitrate_image.resize((30, 80),
                                         Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
    photo = ImageTk.PhotoImage(bitrate_image)
    Bitrate_out = tk.Label(initial_screen, image=photo, borderwidth=0,compound="center",highlightthickness = 0,padx=0,pady=0)
    Bitrate_out.grid(row=4, column=1, rowspan=2, sticky=E)

    Bitrate_out.image = photo



    send_bitrate_txt = tk.Label(initial_screen, text="Send bitrate", anchor=W,
                          height=1, width=15, bg="black", fg="grey", font=("", 15))
    send_bitrate_txt.grid(row=4, column=2, sticky=NW, padx=20)

    send_bitrate = tk.Label(initial_screen, text="", anchor=W,
                      height=2, width=15, bg="black", fg="white", font=("", 20))
    send_bitrate.grid(row=5, column=2, sticky=NW, padx=20)

    # 2. Receive bitrate
    bitrate_image1 = Image.open(f"additional_images/Lgne2Jaune.png")
    bitrate_image1 = bitrate_image1.resize((30, 80),
                                         Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
    photo1 = ImageTk.PhotoImage(bitrate_image1)
    Bitrate_in = tk.Label(initial_screen, image=photo1, borderwidth=0,compound="center",highlightthickness = 0,padx=0,pady=0)
    Bitrate_in.grid(row=6, column=1, rowspan=2, sticky=E)

    Bitrate_in.image = photo1


    receive_bitrate_txt = tk.Label(initial_screen, text="Receive bitrate", anchor=W,
                             height=1, width=15, bg="black", fg="grey", font=("", 15))
    receive_bitrate_txt.grid(row=6, column=2, sticky=NW, padx=20)

    receive_bitrate = tk.Label(initial_screen, text="", anchor=W,
                         height=2, width=15, bg="black", fg="white", font=("", 20))
    receive_bitrate.grid(row=7, column=2, sticky=NW, padx=20)

    # 3. IPv4 address
    ipv4_txt = tk.Label(initial_screen, text="IPv4 address", anchor=W,
                               height=1, width=20, bg="black", fg="grey", font=("", 15))
    ipv4_txt.grid(row=4, column=3, sticky=NW)
    ipv4 = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=20, bg="black", fg="white", font=("", 20))
    ipv4.grid(row=5, column=3, sticky=NW)

    # 4. IPv6 address
    ipv6_txt = tk.Label(initial_screen, text="IPv6 address", anchor=W,
                         height=1, width=20, bg="black", fg="grey", font=("", 15))
    ipv6_txt.grid(row=6, column=3, sticky=NW)
    ipv6 = tk.Label(initial_screen, text="", anchor=W,
                     height=1, width=20, bg="black", fg="white", font=("", 20))
    ipv6.grid(row=7, column=3, sticky=NW)

    subnet_mask_txt = tk.Label(initial_screen, text="Subnet Mask", anchor=W,
                         height=1, width=20, bg="black", fg="grey", font=("", 15))
    subnet_mask_txt.grid(row=6, column=4, sticky=NW)
    subnet_mask = tk.Label(initial_screen, text="", anchor=W,
                     height=1, width=20, bg="black", fg="white", font=("", 20))
    subnet_mask.grid(row=7, column=4, sticky=NW)

    # STATISTICS_2

    Transmission = tk.Frame(initial_screen, bg="black")
    Transmission.rowconfigure(10)
    Transmission.columnconfigure(2)
    Transmission.grid(row=3, column=5, columnspan=2, sticky=N, padx=30)

    # Heading
    network_head = tk.Label(Transmission, text="Data Transmission Keyboard",
                                  height=1, width=30, bg="black", fg="grey", font=("", 12))
    network_head.grid(row=1, column=1, columnspan=2, pady=10)

    # 1.Bytes_sent
    bytes_sent_txt = tk.Label(Transmission, text=f"Sent", anchor=W,
                                   height=1, width=30, bg="black", fg="white", font=("", 15))
    bytes_sent_txt.grid(row=2, column=1, sticky=NW)

    bytes_sent = tk.Label(Transmission, text="", anchor=E,
                               height=1, width=10, bg="black", fg="white", font=("", 15))
    bytes_sent.grid(row=2, column=2, sticky=NW)

    # 2.Packets_sent
    packets_sent_txt = tk.Label(Transmission, text=f"Packets", anchor=W,
                                  height=1, width=30, bg="black", fg="white", font=("", 15))
    packets_sent_txt.grid(row=3, column=1, sticky=NW)

    packets_sent = tk.Label(Transmission, text="", anchor=E,
                              height=1, width=10, bg="black", fg="white", font=("", 15))
    packets_sent.grid(row=3, column=2, sticky=NW)

    # 3.Errin
    errin_txt = tk.Label(Transmission, text=f"Errors while receiving", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    errin_txt.grid(row=4, column=1, sticky=NW)

    errin = tk.Label(Transmission, text="", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    errin.grid(row=4, column=2, sticky=NW)

    # 4.Dropin
    dropin_txt = tk.Label(Transmission, text=f"Dropped incoming packets", anchor=W,
                         height=1, width=30, bg="black", fg="white", font=("", 15))
    dropin_txt.grid(row=5, column=1, sticky=NW)

    dropin = tk.Label(Transmission, text="", anchor=E,
                     height=1, width=10, bg="black", fg="white", font=("", 15))
    dropin.grid(row=5, column=2, sticky=NW)

    #Blank
    blank = tk.Label(Transmission, text="", anchor=E,
                      height=1, width=10, bg="black", fg="white", font=("", 15))
    blank.grid(row=6, column=2, columnspan=2, sticky=NW)

    # 5.Bytes_received
    bytes_received_txt = tk.Label(Transmission, text=f"Received", anchor=W,
                              height=1, width=30, bg="black", fg="white", font=("", 15))
    bytes_received_txt.grid(row=7, column=1, sticky=NW)

    bytes_received = tk.Label(Transmission, text="", anchor=E,
                          height=1, width=10, bg="black", fg="white", font=("", 15))
    bytes_received.grid(row=7, column=2, sticky=NW)

    # 6.Packets_received
    packets_received_txt = tk.Label(Transmission, text=f"Packets", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    packets_received_txt.grid(row=8, column=1, sticky=NW)

    packets_received = tk.Label(Transmission, text="", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    packets_received.grid(row=8, column=2, sticky=NW)

    # 3.Errout
    errout_txt = tk.Label(Transmission, text=f"Errors while sending", anchor=W,
                         height=1, width=30, bg="black", fg="white", font=("", 15))
    errout_txt.grid(row=9, column=1, sticky=NW)

    errout = tk.Label(Transmission, text="", anchor=E,
                     height=1, width=10, bg="black", fg="white", font=("", 15))
    errout.grid(row=9, column=2, sticky=NW)

    # 4.Dropout
    dropout_txt = tk.Label(Transmission, text=f"Dropped outgoing packets", anchor=W,
                          height=1, width=30, bg="black", fg="white", font=("", 15))
    dropout_txt.grid(row=10, column=1, sticky=NW)

    dropout = tk.Label(Transmission, text="", anchor=E,
                      height=1, width=10, bg="black", fg="white", font=("", 15))
    dropout.grid(row=10, column=2, sticky=NW)


    # Average
    Network_average = tk.Frame(initial_screen, bg="black")
    Network_average.rowconfigure(3)
    Network_average.columnconfigure(2)
    Network_average.grid(row=6, column=5, rowspan=3, columnspan=2, sticky=N, padx=30, pady=30)

    # Heading
    heading_stats = tk.Label(Network_average, text="Average Data Transfer Rate",
                             height=1, width=30, bg="black", fg="grey", font=("", 12))
    heading_stats.grid(row=1, column=1, columnspan=2, pady=10)

    # Sent_average
    sent_average_txt = tk.Label(Network_average, text=f"Send: ", anchor=W,
                                height=1, width=30, bg="black", fg="white", font=("", 15))
    sent_average_txt.grid(row=2, column=1, sticky=NW)

    sent_average = tk.Label(Network_average, text=f"", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))
    sent_average.grid(row=2, column=2, sticky=NW)

    # Receive_average
    receive_average_txt = tk.Label(Network_average, text=f"Receive: ", anchor=W,
                                 height=1, width=30, bg="black", fg="white", font=("", 15))
    receive_average_txt.grid(row=3, column=1, sticky=NW)

    receive_average = tk.Label(Network_average, text=f"", anchor=E,
                             height=1, width=10, bg="black", fg="white", font=("", 15))
    receive_average.grid(row=3, column=2, sticky=NW)


# PROLONGEMENT DU PROJET non terminé encore, portant sur l'analyse séparée des processus en temps réel
def processus():
    """
    Procédure qui permet de créer une nouvelle page (Processes), où dans un tableau sont affichées,
    puis filtrées les données relatives à chaque processus tournant dans le système.
    """
    for widget in initial_screen.winfo_children():
        widget.destroy()

    main_menu = tk.Frame(initial_screen, bg="grey")
    main_menu.grid(row=0, column=0, sticky=NW)
    processes = tk.Button(main_menu, text="Processes", fg="white", bg="black")
    processes.grid(row=0, column=0)

    performance = tk.Button(main_menu, text="Performance", command=lambda: cpu_statistics(),
                            fg="white", bg="black")
    performance.grid(row=0, column=1)

    app_history = tk.Button(main_menu, text="App history", fg="white", bg="black")
    app_history.grid(row=0, column=2)

    startup = tk.Button(main_menu, text="Startup", fg="white", bg="black")
    startup.grid(row=0, column=3)

    users = tk.Button(main_menu, text="Users", fg="white", bg="black")
    users.grid(row=0, column=4)

    details = tk.Button(main_menu, text="Details", fg="white", bg="black")
    details.grid(row=0, column=5)

    services = tk.Button(main_menu, text="Services", fg="white", bg="black")
    services.grid(row=0, column=6)


    menu = tk.Frame(initial_screen, bg="grey")
    menu.grid(row=1, column=0, pady=20, columnspan=10)


    style = ttk.Style()
    style.configure("mystyle.Treeview", highlightthickness=0, bd=0, font=('Arial', 11))  # Modify the font of the body
    style.configure("mystyle.Treeview.Heading", font=('Arial', 11, 'bold'))  # Modify the font of the headings
    style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

    table = ttk.Treeview(menu, columns=('Process name', 'Memory', 'Threads', 'PID', 'User'),
                         show='headings', style="mystyle.Treeview", height=10)
    table.column("Process name", anchor="w")
    table.column("Memory", anchor="e")
    table.column("Threads", anchor="e")
    table.column("PID", anchor="e")
    table.column("User", anchor="e")

    table.heading('Process name', text="Process name")
    table.heading('Memory', text="Memory")
    table.heading('Threads', text="Threads")
    table.heading('PID', text="PID")
    table.heading('User', text="User")
    table.pack(side = LEFT, expand=True, fill="both")

    table.tag_configure('odd', background='#E8E8E8')

    process_info = []
    for proc in psutil.process_iter(['name', 'num_threads', 'pid', 'username', 'memory_info']):
        process_info.append(proc.info)

    for i in range(len(process_info)):
        if i % 2 == 0:
            table.insert(parent='', index=0, values=(process_info[i]['name'],
                                                     bytes_to_human_readable(process_info[i]['memory_info'][0],
                                                                             suffix="B"),
                                                     process_info[i]['num_threads'], process_info[i]['pid'],
                                                     process_info[i]['username']), )

        else:
            table.insert(parent='', index=0, values=(process_info[i]['name'],
                                                     bytes_to_human_readable(process_info[i]['memory_info'][0],
                                                                             suffix="B"),
                                                     process_info[i]['num_threads'], process_info[i]['pid'],
                                                     process_info[i]['username']),
                         tags=('odd',))

    vsb = ttk.Scrollbar(menu, orient="vertical", command=table.yview)

    vsb.pack(side='right', fill='y')
    table.configure(yscrollcommand=vsb.set)

def yet_to_come():
    messagebox.showinfo("Yet to come :)", "GPU Performance Visualization to be developped very soon! "
                                          "Don't miss out its release :)")
def new_funct():
    messagebox.showinfo("Coming soon :)", "A new functionality available for your PythonTaskManager "
                                          "in the days to come!")






#===========================================MAIN_PROGRAM===================================================
# Code principal qui définit l'écran d'accueil, puis à partir duquel l'application se lance.


# Définition de l'écran d'accueil
root = tk.Tk()
img = Image.open(fp='additional_images/Logo.png')
logo_img = ImageTk.PhotoImage(img)
root.iconphoto(False, logo_img)
root.title("Python Task Monitor ™")
root.configure(bg="#02041F")
width= root.winfo_screenwidth()
height= root.winfo_screenheight()
root.geometry("%dx%d" % (width, height))

#Detruit la fenêtre Tk si elle est fermée (ne marche pas vraiment :/ )
#root.protocol('WM_DELETE_WINDOW', root.destroy())

initial_screen = tk.Frame(root, bg="black")
initial_screen.pack(fill = "both", expand = True)
initial_screen.rowconfigure(10)
initial_screen.columnconfigure(10)

main_menu = tk.Frame(initial_screen, bg = "black")
main_menu.grid(row=2, column = 0)

start = tk.Button(main_menu, text = "Get started!", fg = "white", bg = "blue", font=("TimesNewRoman","30"),
                      command = lambda: cpu_statistics())
start.grid(row=0,column=0)

logotype = Image.open("additional_images/Presentation.png")
logotype = logotype.resize((1500, 960), Image.Resampling.LANCZOS)  # Utilisation de Image.Resampling.LANCZOS pour le redimensionnement
photo = ImageTk.PhotoImage(logotype)

image_label = tk.Label(initial_screen, image=photo)
image_label.grid(row=0, column=1, columnspan=4, rowspan=4)

logotype.image = photo

welcome = tk.Label(initial_screen, text = "We welcome you on your\n   reliable\n"
                                          "         versatile,\n              user-friendly\n"
                                           "system managing App Nb.1\n         - Python Task Monitor!",
                   font=("", "25"), fg = "orange", bg="black", anchor="w", justify="left")
welcome.grid(row=1, column=0, sticky=W, padx=10)

credentials = tk.Label(initial_screen, text="©PyTaM: All rights reserved", font=("", "15"), fg = "white",
                       bg="black", anchor="nw")
credentials.grid(row=3, column=0, sticky=SW)


"""
Définition de fonctions qui tournent en permanence dans le background de l'application et qui permettent
de les utiliser notamment pour construire les graphiques au sein des fonctions update_plot().
Si ces fonctions n'étaient pas définis et lancées dans le corps principal du code, elle n'auraient pas
fonctionné au sein des fonctions appelées lorsque l'on clique sur des boutons pour lancer une nouvelle
fenêtre et constuire un nouveau graphique. 
"""


# Pour CPU
fig, ax = plt.subplots(figsize=(10, 7), facecolor="black")
fig.subplots_adjust(left=0.075, bottom=0.01, right=0.95, top=0.95, wspace=0, hspace=0)

x = [i for i in range(60, -1, -1)]
y = []

percent_cpu = 0

def update_plot(i, x, y):
    global percent_cpu
    percent = psutil.cpu_percent(1)
    y.append(percent)

    cpu.config(text=f"CPU \n {percent} %")

    CPU_realtime.config(text=f"Latest used \n {percent} %")

    x = x[-61:]
    y = y[-61:]
    x = x[-len(y):]
    y = y[-len(y):]

    ax.clear()
    ax.plot(x, y, color="blue")
    ax.fill_between(x, y, 0, color='blue', alpha=.1)

    ax.spines["bottom"].set_color("white")
    ax.spines["left"].set_color("white")
    ax.tick_params(axis="x", colors="white")
    ax.tick_params(axis="y", colors="white")
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.title.set_color('white')
    ax.set_ylim([0, 101])
    ax.set_xlim([60, 0])
    ax.set_facecolor("black")

    plt.grid(color='c', linewidth=0.2)

    plt.subplots_adjust(bottom=0.20)
    ax.set_xlabel('Time Period, s')
    ax.set_ylabel('CPU Utilization, %')

anim = FuncAnimation(fig, update_plot, fargs=(x, y), interval=250)


# Pour Memory
fig, ax = plt.subplots(figsize=(9, 7), facecolor="black")
fig.subplots_adjust(left=0.075, bottom=0.07, right=0.95, top=0.95, wspace=0, hspace=0)

x = [i for i in range(60, -1, -1)]
y = []

def update_plot(i, x, y):
    percent = psutil.virtual_memory()[2]
    y.append(percent)

    x = x[-61:]
    y = y[-61:]
    x = x[-len(y):]
    y = y[-len(y):]

    ax.clear()
    ax.plot(x, y, color="blue")
    ax.fill_between(x, y, 0, color='blue', alpha=.1)

    ax.spines["bottom"].set_color("white")
    ax.spines["left"].set_color("white")
    ax.tick_params(axis="x", colors="white")
    ax.tick_params(axis="y", colors="white")
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.title.set_color('white')
    ax.set_ylim([0, 101])
    ax.set_xlim([60, 0])
    ax.set_facecolor("black")

    plt.grid(color='c', linewidth=0.2)

    plt.subplots_adjust(bottom=0.10)

    ax.set_xlabel('Time Period, s')
    ax.set_ylabel('RAM Used, %')

anim1 = FuncAnimation(fig, update_plot, fargs=(x, y), interval=250)


# Pour Disk (percentage)
fig, ax = plt.subplots(figsize=(9, 7), facecolor="black")
fig.subplots_adjust(left=0.075, bottom=0.07, right=0.95, top=0.95, wspace=0, hspace=0)

x = [i for i in range(60, -1, -1)]
y = []

def update_plot(i, x, y):
    percent_disk = psutil.disk_usage("C:\\")[3]
    y.append(percent_disk)

    x = x[-61:]
    y = y[-61:]
    x = x[-len(y):]
    y = y[-len(y):]

    ax.clear()
    ax.plot(x, y, color="blue")
    ax.fill_between(x, y, 0, color='blue', alpha=.1)

    ax.spines["bottom"].set_color("white")
    ax.spines["left"].set_color("white")
    ax.tick_params(axis="x", colors="white")
    ax.tick_params(axis="y", colors="white")
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.title.set_color('white')
    ax.set_ylim([0, 101])
    ax.set_xlim([60, 0])
    ax.set_facecolor("black")

    plt.grid(color='c', linewidth=0.2)

    plt.subplots_adjust(bottom=0.10)

    ax.set_xlabel('Time Period, s')
    ax.set_ylabel('RAM Used, %')

anim2 = FuncAnimation(fig, update_plot, fargs=(x, y), interval=250)


# Disk transfer
fig, ax = plt.subplots(figsize=(9, 7), facecolor="black")
fig.subplots_adjust(left=0.075, bottom=0.07, right=0.95, top=0.95, wspace=0, hspace=0)

x1 = [i for i in range(60, -1, -1)]
y1 = []

x2 = [i for i in range(60, -1, -1)]
y2 = []

def update_plot(i, x1, y1, x2, y2):
    read_bytes = round(psutil.disk_io_counters()[2] / 10 ** 6, 1)
    read_time = psutil.disk_io_counters()[4] / 10 ** 3
    read_rate = round(read_bytes / read_time, 2)
    y1.append(read_rate)

    write_bytes = round(psutil.disk_io_counters()[3] / 10 ** 6, 1)
    write_time = psutil.disk_io_counters()[5] / 10 ** 3
    write_rate = round(write_bytes / write_time, 2)
    y2.append(write_rate)

    x1 = x1[-61:]
    y1 = y1[-61:]
    x1 = x1[-len(y):]
    y1 = y1[-len(y):]

    x2 = x2[-61:]
    y2 = y2[-61:]
    x2 = x2[-len(y):]
    y2 = y2[-len(y):]

    ax.clear()
    ax.plot(x1, y1, color="green")
    ax.fill_between(x1, y1, 0, color='green', alpha=.1)
    ax.plot(x2, y2, color = "red")

    ax.spines["bottom"].set_color("white")
    ax.spines["left"].set_color("white")
    ax.tick_params(axis="x", colors="white")
    ax.tick_params(axis="y", colors="white")
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.title.set_color('white')
    ax.set_ylim([0, 101])
    ax.set_xlim([60, 0])
    ax.set_facecolor("black")

    plt.grid(color='c', linewidth=0.2)

    plt.subplots_adjust(bottom=0.20)

    ax.set_xlabel('Time Period, s')
    ax.set_ylabel('Disk Transfer Rate, %')

anim3 = FuncAnimation(fig, update_plot, fargs=(x1, y1, x2, y2), interval=250)



anim2 = FuncAnimation(fig, update_plot, fargs=(x, y, x1, y1, x2, y2), interval=250)



"""
Définition des labels globaux qui permettent ensuite les modifier au sein des fonctions update_plot()
lorsque l'on construit les graphique et "update" les valeurs de ces labels.
"""


CPU_realtime_txt = tk.Label(initial_screen, text = "Latest",
                        height = 1, width = 20, bg ="black", fg ="white", font = ("", 12))
CPU_realtime = tk.Label(initial_screen, text = "Latest",
                        height = 1, width = 20, bg ="black", fg ="white", font = ("", 12))

CPU_speed_txt = tk.Label(initial_screen, text = "Latest",
                        height = 1, width = 20, bg ="black", fg ="white", font = ("", 12))
CPU_speed = tk.Label(initial_screen, text = "Latest",
                        height = 1, width = 20, bg ="black", fg ="white", font = ("", 12))

CPU_threads_txt = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))
CPU_threads = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))

CPU_processes_txt = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))
CPU_processes = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))

CPU_handles_txt = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))
CPU_handles = tk.Label(initial_screen, text = "Threads",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))

CPU_cores = tk.Label(initial_screen, text = "Performance per CPU",
                       height = 1, width = 20, bg = "black", fg = "white", font=("", 12))

menu = tk.Frame(initial_screen, bg="grey")


cpu = tk.Button(menu, text=f"CPU", font=("", "15"), anchor=W, width=20, height=2,
                command=lambda: cpu_statistics(), fg = "white", bg = "black")

memory = tk.Button(menu, text="Memory", font=("", "15"), anchor=W, width=20, height=2,
                       command = lambda: memory_statistics(), fg = "white", bg = "black")

disk = tk.Button(menu, text="Disk", font=("", "15"), anchor=W, width=20, height=2,
                       command = lambda: disk_statistics(), fg = "white", bg = "black")

network = tk.Button(menu, text="Network", font=("", "15"), anchor=W, width=20, height=2,
                       command = lambda: network_statistics(), fg = "white", bg = "black")

gpu = tk.Button(menu, text="GPU", font=("", "15"), anchor=W, width=307, height=90,
                        fg="white", bg="black", command=lambda: network_statistics())


Timer = tk.Label(initial_screen, text = "")
Battery = tk.Label(initial_screen, text = "")

# Pour CPU
processes = 1
numThreads = 0
handles = 0

CPU_cores = tk.Frame(initial_screen, bg="black")
CPU_cores.rowconfigure(2 + psutil.cpu_count())
CPU_cores.columnconfigure(2)

cores_txt = []
cores = []
for i in range(psutil.cpu_count()):
    cores_txt.append(tk.Label(CPU_cores, text=f"Core {i+1}",
                        height=1, width=20, bg="black", fg="white", font=("", 12)))
    cores.append(tk.Label(CPU_cores, text="",
                        height=1, width=20, bg="black", fg="white", font=("", 12)))

#TRIVIAL INFO_CPU
Trivia_CPU = tk.Frame(initial_screen, bg="black")
Trivia_CPU.rowconfigure(4)
Trivia_CPU.columnconfigure(2)

num_switches = tk.Label(Trivia_CPU, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

num_interrupts = tk.Label(Trivia_CPU, text=f"", anchor=E,
                          height=1, width=10, bg="black", fg="white", font=("", 15))

num_syscalls = tk.Label(Trivia_CPU, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

cpu_average = tk.Label(Trivia_CPU, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

CPU_uptime = tk.Label(initial_screen, text="", anchor=W,
                          height=1, width=10, bg="black", fg="grey", font=("", 20))

#MEMORY
in_use = tk.Label(initial_screen, text="", anchor=W,
                  height=2, width=10, bg="black", fg="white", font=("", 20))
in_use.grid(row=5, column=1, sticky=NW)

available = tk.Label(initial_screen, text="", anchor=W,
                     height=2, width=10, bg="black", fg="white", font=("", 20))
available.grid(row=5, column=2, sticky=NW)

percent_ram = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=10, bg="black", fg="white", font=("", 20))
percent_ram.grid(row=7, column=1, sticky=NW)

total = tk.Label(initial_screen, text="", anchor=W,
                             height=1, width=10, bg="black", fg="white", font=("", 20))
total.grid(row=7, column=2, sticky=NW)

free = tk.Label(initial_screen, text="", anchor=W,
                           height=1, width=10, bg="black", fg="white", font=("", 20))
free.grid(row=7, column=3, sticky=NW)

#TRIVIA_Memory
Trivia_RAM = tk.Frame(initial_screen, bg="black")
Trivia_RAM.rowconfigure(7)
Trivia_RAM.columnconfigure(2)
Trivia_RAM.grid(row=3, column=4, columnspan=2, sticky=N, padx=30)

total_swap = tk.Label(Trivia_RAM, text="", anchor=E,
                      height=1, width=10, bg="black", fg="white", font=("", 15))

# 2.Used
swap_used = tk.Label(Trivia_RAM, text="", anchor=E,
                     height=1, width=10, bg="black", fg="white", font=("", 15))

# 3.Percent_used
swap_percent = tk.Label(Trivia_RAM, text="", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

# 4.Free
swap_free = tk.Label(Trivia_RAM, text="", anchor=E,
                     height=1, width=10, bg="black", fg="white", font=("", 15))

# 5.Sin (bytes)
swap_sin = tk.Label(Trivia_RAM, text="", anchor=E,
                    height=1, width=10, bg="black", fg="white", font=("", 15))

# 6.Sout (bytes)
swap_sout = tk.Label(Trivia_RAM, text="", anchor=E,
                     height=1, width=10, bg="black", fg="white", font=("", 15))

#Average_memory
Average_stat = tk.Frame(initial_screen, bg="black")
Average_stat.rowconfigure(2)
Average_stat.columnconfigure(2)

average_percent = tk.Label(Average_stat, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))


#DISK
Disk_used = tk.Label(initial_screen, text="", anchor=W,
                     height=2, width=10, bg="black", fg="white", font=("", 20))

Disk_total = tk.Label(initial_screen, text="", anchor=W,
                      height=2, width=10, bg="black", fg="white", font=("", 20))

Disk_percent = tk.Label(initial_screen, text="", anchor=W,
                        height=2, width=10, bg="black", fg="white", font=("", 20))

Disk_free = tk.Label(initial_screen, text="", anchor=W,
                     height=1, width=10, bg="black", fg="white", font=("", 20))

#Disk_stat_2
Disk_transfer = tk.Frame(initial_screen, bg="black")
Disk_transfer.rowconfigure(8)
Disk_transfer.columnconfigure(2)

disk_read_speed = tk.Label(Disk_transfer, text="", anchor=E,
                           height=1, width=10, bg="black", fg="white", font=("", 15))

disk_read_count = tk.Label(Disk_transfer, text="", anchor=E,
                           height=1, width=10, bg="black", fg="white", font=("", 15))

disk_read_time = tk.Label(Disk_transfer, text="", anchor=E,
                          height=1, width=10, bg="black", fg="white", font=("", 15))

disk_write_speed = tk.Label(Disk_transfer, text="", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))

disk_write_count = tk.Label(Disk_transfer, text="", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))

disk_write_time = tk.Label(Disk_transfer, text="", anchor=E,
                           height=1, width=10, bg="black", fg="white", font=("", 15))

#Disk average
# Average
Disk_average = tk.Frame(initial_screen, bg="black")
Disk_average.rowconfigure(3)
Disk_average.columnconfigure(2)

read_average = tk.Label(Disk_average, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

write_average = tk.Label(Disk_average, text=f"", anchor=E,
                         height=1, width=10, bg="black", fg="white", font=("", 15))

# STATISTICS_Network

# 1. Send bitrate
send_bitrate = tk.Label(initial_screen, text="", anchor=W,
                        height=2, width=10, bg="black", fg="white", font=("", 20))

receive_bitrate = tk.Label(initial_screen, text="", anchor=W,
                           height=2, width=10, bg="black", fg="white", font=("", 20))

ipv4 = tk.Label(initial_screen, text="", anchor=W,
                height=1, width=10, bg="black", fg="white", font=("", 20))

ipv6 = tk.Label(initial_screen, text="", anchor=W,
                 height=1, width=10, bg="black", fg="white", font=("", 20))

subnet_mask = tk.Label(initial_screen, text="", anchor=W,
                       height=1, width=20, bg="black", fg="white", font=("", 20))
subnet_mask.grid(row=7, column=3, sticky=NW)

# STATISTICS_2

Transmission = tk.Frame(initial_screen, bg="black")
Transmission.rowconfigure(10)
Transmission.columnconfigure(2)


bytes_sent = tk.Label(Transmission, text="", anchor=E,
                      height=1, width=10, bg="black", fg="white", font=("", 15))

packets_sent = tk.Label(Transmission, text="", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

errin = tk.Label(Transmission, text="", anchor=E,
                 height=1, width=10, bg="black", fg="white", font=("", 15))

dropin = tk.Label(Transmission, text="", anchor=E,
                  height=1, width=10, bg="black", fg="white", font=("", 15))

bytes_received = tk.Label(Transmission, text="", anchor=E,
                          height=1, width=10, bg="black", fg="white", font=("", 15))

packets_received = tk.Label(Transmission, text="", anchor=E,
                            height=1, width=10, bg="black", fg="white", font=("", 15))

errout = tk.Label(Transmission, text="", anchor=E,
                  height=1, width=10, bg="black", fg="white", font=("", 15))

dropout = tk.Label(Transmission, text="", anchor=E,
                   height=1, width=10, bg="black", fg="white", font=("", 15))

# Average
Network_average = tk.Frame(initial_screen, bg="black")
Network_average.rowconfigure(3)
Network_average.columnconfigure(2)


sent_average = tk.Label(Network_average, text=f"", anchor=E,
                        height=1, width=10, bg="black", fg="white", font=("", 15))

receive_average = tk.Label(Network_average, text=f"", anchor=E,
                           height=1, width=10, bg="black", fg="white", font=("", 15))


tk.mainloop()
